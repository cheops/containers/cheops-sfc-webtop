FROM lscr.io/linuxserver/webtop:amd64-fedora-xfce

# Build arguments/variables

ARG MPS_VERSION=11.16.0
ARG INSTALL_MPS_CLIENT=no
ARG BUILD_DATE
ARG BUILD_VERSION

ARG PROMPT='[\u@cheops-sfc \W]\$ '

ARG USER_NAME=abc
ARG GROUP_NAME=abc
ARG USER_UID=911
ARG USER_GID=911
ARG USER_HOME=/config

# The work directory for building the docker container
ARG BUILD_DIR=/tmp/build
# Directory containing external files
ARG CUSTOM_DIR=/custom
ARG MPS_SRC_DIR=$CUSTOM_DIR/mps/mps-$MPS_VERSION
# If this directory exists, the files in it will be copied into $MPS_INPUT_TRAY/
ARG MPS_INPUT_FILES=$MPS_SRC_DIR/mps-input-tray

# Environment variables

# The title of the webtop web page
ENV TITLE="CHEOPS SFC"
ENV DISPLAY=:1.0

ENV MPS_VERSION=$MPS_VERSION
ENV MPS_INSTALL_DIR=/opt/mps
ENV MPS_INPUT_TRAY=${MPS_INSTALL_DIR}/mps-input-tray
ENV MPS_OUTPUT_TRAY=${MPS_INSTALL_DIR}/mps-output-tray
ENV MPS_LOCAL_INPUT_TRAY=${MPS_INSTALL_DIR}/mps-local-input-tray

ENV PATH=$MPS_INSTALL_DIR/mps-server-$MPS_VERSION/bin:$PATH
ENV PATH=$MPS_INSTALL_DIR/mps-hmi-fc-$MPS_VERSION/bin:$PATH

ENV JAVA_HOME=/etc/alternatives/java_sdk
ENV JRE_HOME=/etc/alternatives/jre

ENV MAVEN_OPTS=""

ENV PG_USER=postgres
ENV PG_PASS=postgres
ENV PG_VERSION=16
ENV PG_PREFIX=/usr

# Image labels

LABEL cheops.image.created=$BUILD_DATE
LABEL cheops.image.version=$BUILD_VERSION
LABEL cheops.image.description="CHEOPS Scheduling Feasibility Checker (SFC)"
LABEL cheops.image.maintainer="CHEOPS Science Operations Centre"
LABEL cheops.image.url="https://gitlab.unige.ch/cheops/containers/cheops-sfc-webtop"
LABEL cheops.image.sfc.version=$MPS_VERSION

COPY root/ /

SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]

WORKDIR $BUILD_DIR

RUN \
# Exit with error if no MPS version is specified
test -n "$MPS_VERSION" || (echo "MPS_VERSION is not set" && exit 1); \
################################################################################
#
# System packages and global configuration
#
yum -y install \
    geany \
    gedit \
    java-1.8.0-openjdk \
    java-1.8.0-openjdk-devel \
    libgeotiff \
    libgomp \
    maven-openjdk8 \
    sudo \
    unzip \
    vim \
    wget \
    xterm \
    zip; \
global_profile=/etc/profile.d/cheops-sfc.sh; \
echo "export PS1='$PROMPT'" >> $global_profile; \
echo "export MAVEN_OPTS=''" >> $global_profile; \
echo "export PATH=$MPS_INSTALL_DIR/mps-server-$MPS_VERSION/bin:\$PATH" >> $global_profile; \
echo "export PATH=$MPS_INSTALL_DIR/mps-hmi-fc-$MPS_VERSION/bin:\$PATH" >> $global_profile; \
echo "export MPS_VERSION=$MPS_VERSION" >> $global_profile; \
echo "export PG_USER=$PG_USER" >> $global_profile; \
echo "export PG_PREFIX=$PG_PREFIX" >> $global_profile; \
echo "export JAVA_HOME=$JAVA_HOME" >> $global_profile; \
echo "export JRE_HOME=$JRE_HOME" >> $global_profile; \
################################################################################
#
# Postgresql
#
dnf -qy module disable --skip-unavailable postgresql; \
dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm; \
dnf install -y postgresql${PG_VERSION} postgresql${PG_VERSION}-server; \
# Prevent dnf from asking for confirmation to import GPG keys for the postgres
# repositories
sed -i 's/repo_gpgcheck = 1/repo_gpgcheck = 0/' /etc/yum.repos.d/pgdg-redhat-all.repo; \
# postgres failes unless the following directory exists and is owned by the 
# postgres user
mkdir /var/run/postgresql; \
chown $PG_USER:$PG_USER /var/run/postgresql; \
su - $PG_USER -c "$PG_PREFIX/bin/initdb"; \
su - $PG_USER -c "$PG_PREFIX/bin/pg_ctl -l \$PGDATA/serverlog start"; \
psql -U $PG_USER -c "ALTER USER $PG_USER PASSWORD '$PG_PASS'"; \
# Modify pg_hba.conf according to the MPS installation manual:
# Add a line for the postgres user
su - $PG_USER -c "sed '/^local.*all.*all/i local   all             postgres                                md5' -i \$PGDATA/pg_hba.conf"; \
# Replace trust with md5 for all except for 'local all all'
su - $PG_USER -c "sed '/^host/ s/trust/md5/' -i \$PGDATA/pg_hba.conf"; \
su - $PG_USER -c "sed '/^local.*replication/ s/trust/md5/' -i \$PGDATA/pg_hba.conf"; \
################################################################################
#
# MPS
#
mkdir -p ${MPS_INSTALL_DIR}; \
su $PG_USER -c "$PG_PREFIX/bin/createdb -U '$PG_USER' cheops_mps"; \
# Install the MPS and SFC
set +e; \
yes | $MPS_SRC_DIR/install_mps-server.bin; \
yes | $MPS_SRC_DIR/install_mps-hmi-fc.bin; \
set -e; \
# Note: the MPS client and server log paths in the mps.gobal.properties files
# included in the installers get overwritten during the installation. For this
# reason, we copy the correct properties files into the installation directories
# afterwards.
cp $MPS_SRC_DIR/mps.global.properties $MPS_INSTALL_DIR/mps-server-$MPS_VERSION/config/; \
cp $MPS_SRC_DIR/mps.global.properties $MPS_INSTALL_DIR/mps-hmi-fc-$MPS_VERSION/config/; \
# Set proper file permissions.
# Change permissions of all files to -rw-r--r--
find $MPS_INSTALL_DIR -type f -print0 |xargs -0 chmod 644; \
# Change permissions of all directories to drwxr-xr-x
find $MPS_INSTALL_DIR -type d -print0 |xargs -0 chmod 755; \
# Set the executable bit on the server and sfc binaries
chmod ugo+x $MPS_INSTALL_DIR/mps-server-$MPS_VERSION/bin/* $MPS_INSTALL_DIR/mps-hmi-fc-$MPS_VERSION/bin/* $MPS_INSTALL_DIR/mps-hmi-fc-$MPS_VERSION/lib/mps-hmi-fc-product/CHEOPS-SOC-MPS; \
if [ "$INSTALL_MPS_CLIENT" == "yes" ]; then \
    echo "export PATH=$MPS_INSTALL_DIR/mps-hmi-$MPS_VERSION/bin:\$PATH" >> $global_profile; \
    set +e; \
    yes | $MPS_SRC_DIR/install_mps-hmi.bin; \
    set -e; \
    cp $MPS_SRC_DIR/mps.global.properties $MPS_INSTALL_DIR/mps-hmi-$MPS_VERSION/config/; \
    chmod ugo+x $MPS_INSTALL_DIR/mps-hmi-$MPS_VERSION/bin/* $MPS_INSTALL_DIR/mps-hmi-$MPS_VERSION/lib/mps-hmi-product/CHEOPS-SOC-MPS; \
    if [ -d $MPS_INPUT_FILES ]; then \
        mv $MPS_INPUT_FILES $MPS_INPUT_TRAY; \
    fi; \
fi; \
mkdir -p /tmp/cheops-mps; \
# Ensure that the MPS can write the logs regardless of which user is used to start it
chmod -R go+rw /tmp/cheops-mps; \
touch /tmp/cheops-mps/default.log; \
chmod go+rw /tmp/cheops-mps/default.log; \
# Restore a database dump of the cheops_mps database. Uncomment the following line
# to use a plain sql dump instead of a "custom" format dump.
#psql -U $PG_USER cheops_mps < $MPS_SRC_DIR/cheops_mps_db_dump.sql > /dev/null; \
pg_restore -U $PG_USER -d cheops_mps -j 8 $MPS_SRC_DIR/cheops_mps_db_dump.custom; \
su - $PG_USER -c "$PG_PREFIX/bin/pg_ctl stop -l \$PGDATA/serverlog -D \$PGDATA/"; \
sed -i "s/mps_version=\$MPS_VERSION/mps_version='$MPS_VERSION'/" /usr/bin/cheops-sfc; \
#
# Clean up
#
rm -rf $CUSTOM_DIR/mps;

WORKDIR /