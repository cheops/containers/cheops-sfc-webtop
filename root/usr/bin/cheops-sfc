#!/bin/bash

set -o pipefail 

script_name=${0##*/}

log_dir=$HOME/logs
log_file=$log_dir/cheops-sfc.log

sfc_start_process=mps-hmi-fc-product

# MPS_VERSION is set in the Dockerfile
mps_version=$MPS_VERSION

mps_start_process=CHEOPS-SOC-MPS-SERVER
mps_server_process=pt.com.deimos.cheops.soc.mps.ws.server.Server
mps_server_log=$HOME/.mps.log
mps_timeout=15

usage() {
    printf "Usage: %s [-v]\n\n" $script_name
}

description() {
    printf "Starts the CHEOPS Scheduling Feasibility Checker (SFC).\n"
    printf "\n"
    printf "Parameters:\n"
    printf "  -h           Print this help and exit.\n"
    printf "  -v           Print the version and exit\n"
}

debug() {
    printf "%s %s: [I] %s\n" "$(date +"%Y-%m-%d %T")" "$script_name" "$@" >> $log_file
}

log() {
    printf "%s %s: [I] %s\n" "$(date +"%Y-%m-%d %T")" "$script_name" "$@" | tee -a $log_file
}

err() {
    printf "%s %s: [E] %s\n" "$(date +"%Y-%m-%d %T")" "$script_name" "$@" | tee -a $log_file 1>&2
}

kill_proc() {
    # Arguments:
    #   $1 - a string matching the process(es) to be killed with pkill
    
    local proc_name=$1
    pkill -9 -f $proc_name
    status=$?
    if [ $status -eq 0 ]; then
        debug "Stopped $proc_name"
    elif [ $status -eq 1 ]; then
        debug "$proc_name is not running"
    else
        err "There was an error stopping $proc_name. pkill returned $status"
    fi
}

kill_mps() {
    kill_proc "$sfc_start_process"
    kill_proc "$mps_start_process"
    kill_proc "$mps_server_process"
}

start_mps() {
    rm -f $mps_server_log
    touch $mps_server_log
    
    log "Starting the MPS server. Please wait..."
    CHEOPS-SOC-MPS-SERVER 2>&1 | tee -a $mps_server_log >> $log_file &
    
    # If the MPS has not started within a certain time, it means the MPS
    # process is hanging.
    #
    # - timeout N makes the command exit if still running after N seconds. We run
    #   the command with bash -c in order for timeout to correctly handle the pipe.
    # - -q makes grep exit on first match.
    # - "Server started..." indicates that the MPS server has successfully started.
    timeout $mps_timeout bash -c "tail -f $mps_server_log | grep -q 'Server started...'"
    # timeout returns 124 if the process timed out
    if [ $? -eq 124 ]; then
        err "MPS server start timed out after ${mps_timeout}s"
        err "Failed to start the MPS server withing ${mps_timeout} sec. Retrying - please wait..."
        kill_mps
    fi
}

while getopts ":hv" arg; do
    case $arg in
        h)  usage
            description
            exit
            ;;
        v)  echo "CHEOPS SFC $mps_version"
            exit
            ;;
        *)  printf "Invalid argument '%s'\n" $OPTARG
            usage 1>&2
            exit 1
            ;;
    esac
done

# If the MPS server is already running, stop it in case the process is hanging
mps_pid=$(pgrep -f $mps_server_process)
if [ $? -eq 0 ] && [ -n $mps_pid ]; then
    log "Stopping a pre-existing MPS server..."
    kill_mps
fi

start_mps

# Check if the MPS server is running
mps_pid=$(pgrep -f $mps_server_process)
if [ $? -ne 0 ]; then
    err "Retrying..."
    start_mps
fi

# Give up...
mps_pid=$(pgrep -f $mps_server_process)
if [ $? -ne 0 ]; then
    err "MPS server is not running. Exiting..."
    exit 1
fi

mkdir -p $log_dir

echo ""
log "Starting the SFC. Please wait..."
CHEOPS-SOC-MPS-FC &>> $log_file

log "SFC stopped"

kill_mps
