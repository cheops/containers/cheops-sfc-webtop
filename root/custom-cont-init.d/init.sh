#!/bin/bash

set -e

echo "--------------------------------------------------------------------------------"
echo "CHEOPS SFC custom files START"
echo ""

home=/config
desktop_dir=$home/Desktop
autostart_dir=$home/.config/autostart
username=abc
custom_dir=/custom
init_file=$home/.config/sfc-initialized

sfc_input_dir=$home/sfc-input
sfc_output_dir=$home/sfc-output
log_dir=$home/logs

function init() {
    # Copy all customizations
    mkdir -p $desktop_dir
    mkdir -p $sfc_input_dir
    mkdir -p $sfc_output_dir
    mkdir -p $log_dir
    mkdir -p $autostart_dir
    
    cp $custom_dir/home/.bashrc $home/
    cp $custom_dir/home/.bash_profile $home/

    cp -r $custom_dir/home/Desktop/* $desktop_dir/
    cp $custom_dir/home/sfc-input/* $sfc_input_dir/
    cp $custom_dir/home/.config/autostart/*.desktop $autostart_dir/;

    # Ensure that SFC desktop shortcut is executable
    chmod +x $desktop_dir/sfc*.desktop

    # Create SFC menu item
    cp $custom_dir/home/Desktop/sfc.desktop /usr/share/applications/
    chmod -x /usr/share/applications/sfc.desktop

    # Create Gedit desktop shortcut
    #cp /usr/share/applications/org.gnome.gedit.desktop $desktop_dir/;
    #chmod +x $desktop_dir/org.gnome.gedit.desktop; 
    cp /usr/share/applications/geany.desktop $desktop_dir/;
    chmod +x $desktop_dir/geany.desktop; 

    chown -R $username:$username $home;

    touch $init_file
}

if [ -f $init_file ]; then
    echo "Skipping customization: already initialized"
    # The autostart .desktop file has been modified. Ensure that the file
    # is updated even if the cheops-sfc directory already exists.
    mkdir -p $autostart_dir
    cp $custom_dir/home/.config/autostart/*.desktop $autostart_dir/;
else
    set -x
    init
    set +x
fi

# Start postgres
echo "Starting postgres"
su - postgres -c "pg_ctl start -l \$PGDATA/serverlog -D \$PGDATA/"

# The MPS server is currently started automatically when the user clicks on the
# SFC icon in the desktop. To instead start the MPS when the container starts,
# uncomment the following.
# sleep 2
# echo "Starting the MPS server"
# nohup CHEOPS-SOC-MPS-SERVER &
# sleep 6

echo ""
echo "CHEOPS SFC custom files END"
echo "--------------------------------------------------------------------------------"
