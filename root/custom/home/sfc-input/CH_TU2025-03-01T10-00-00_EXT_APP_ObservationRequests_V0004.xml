<?xml version="1.0" encoding="UTF-8"?>
<!--                                                                                     -->
<!--                                                                                     -->
<!--                                                                                     -->
<!--                                                                                     -->
<!--         Template Observation Request file v. 11.16.0                                -->
<!--                                                                                     -->
<!--         This file can be ingested in the CHEOPS Feasibility Checker v11.16.0        -->
<!--                                                                                     -->
<!--                                                                                     -->
<!--         This version of the file contains an example of                             -->
<!--          how to set up a non-time-critical observation                              -->
<!--                                                                                     -->
<!--                                                                                     -->
<!--         ONLY EDIT LINES PRECEDED by ########################                        -->
<!--                                                                                     -->
<!--                                                                                     -->
<!--         If you edit lines NOT preceded by ##########################                -->
<!--         the ingestion of this file in the Feasibility Checker might fail            -->
<!--          (the order of the parameters/lines matters)                                -->
<!--                                                                                     -->
<!--                                                                                     -->
<!--         File prepared by CHEOPS SOC - UGE/ABE - NBI - Feb. 20, 2025                 -->
<!--                                                                                     -->
<!--                                                                                     -->
<Earth_Explorer_File xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="ext_app_observation_requests_schema.xsd">
  <Earth_Explorer_Header>
    <Fixed_Header>
      <File_Name>CH_TU2025-03-01T10-00-00_EXT_APP_ObservationRequests_V0004</File_Name>
      <File_Description>Observation requests file</File_Description>
      <Notes>Template file for CHEOPS observation Request : FeasibilityChecker (phase-1) and PHT2 (phase-2)</Notes>
      <Mission>CHEOPS</Mission>
      <File_Class>TEST</File_Class>
      <File_Type>EXT_APP_ObservationRequests</File_Type>
      <Validity_Period>
        <Validity_Start>UTC=2025-03-01T00:00:00</Validity_Start>
        <Validity_Stop>UTC=2028-12-31T00:00:00</Validity_Stop>
      </Validity_Period>
      <File_Version>0004</File_Version>
      <Source>
        <System>PSO</System>
        <Creator>PHT2</Creator>
        <Creator_Version>000</Creator_Version>
        <Creation_Date>UTC=2025-02-20T00:00:00</Creation_Date>
      </Source>
    </Fixed_Header>
    <Variable_Header>
      <Programme_Type>10</Programme_Type>
    </Variable_Header>
  </Earth_Explorer_Header>
  
  <Data_Block type="xml">

    <!-- NON-TIME-CRITICAL REQUEST -->
    <List_of_Non_Time_Critical_Requests count="1">
            <Non_Time_Critical_Request>
                <Programme_ID>1</Programme_ID>
                
				<Observation_Request_ID>1</Observation_Request_ID>
                
				<Observation_Category>non time critical</Observation_Category>
                
				<Proprietary_Period_First_Visit unit="days">547</Proprietary_Period_First_Visit>
                
                <Proprietary_Period_Last_Visit unit="days">365</Proprietary_Period_Last_Visit>

                <!--  #############################   Set the target name                                                                      -->
				<Target_Name>Your target name</Target_Name>
              
                <Gaia_ID>GAIA DR2 4284033099545147008</Gaia_ID>
                
                <Spectral_Type>G9V</Spectral_Type>
                
                <!--  #############################   Set the target Gaia band magnitude (relevant to compute straylight contamination levels)    -->
	         	<Target_Magnitude unit="mag">12.0</Target_Magnitude>
                
                <Target_Magnitude_Error unit="mag">0.06</Target_Magnitude_Error>

                <Readout_Mode>faint</Readout_Mode>

                <!--  #############################   Set the target Right Ascension, ICRS coord. (ep=J2000)                                   -->
                <Right_Ascension unit="deg">279.6413208</Right_Ascension>
                
                <!--  #############################   Set the target Declinaison, ICRS coord. (ep=J2000)                                       -->
                <Declination unit="deg">5.6246583</Declination>
                
                <RA_Proper_Motion unit="mas/year">-4.1</RA_Proper_Motion>
                
                <DEC_Proper_Motion unit="mas/year">-10.4</DEC_Proper_Motion>
                
                <Parallax unit="mas">81.03</Parallax>
                
                <T_Eff unit="Kelvin">5225</T_Eff>
                
                <Extinction unit="mag">0.1</Extinction>
                
                    <!--  Optional                                                                                                             -->
                    <!--  The following 2 parameters are used to bound the period when the visits should be scheduled                          -->
                                       <!--  e.g. 01 Mar 2025 = 2460735.5 (JD) -->
                    <!--  These 2 parameters are optional and can be commented out if not relevant for this observation                        -->
                <!--  #############################   Set the earliest start date (in BJD) for this observation request                        -->
                <Earliest_Start unit="BJD">2460735.5</Earliest_Start>
                <!--  #############################   Set the latest end date (in BJD) for this observation request                          -->
                <!--
                <Latest_End unit="BJD">2461000.5</Latest_End>
                -->
                
                <Exposure_Time unit="sec">60</Exposure_Time>
                
				<Number_Stacked_Images>1</Number_Stacked_Images>
								
                <Number_Stacked_Imagettes>0</Number_Stacked_Imagettes>
                
                
                    <!--  1 orbit  =  98.77 minutes = 5926 seconds -->
                <!--  #############################   Set the visit duration (in seconds)                                                      -->
				<Visit_Duration unit="sec">59260</Visit_Duration>


                <Number_of_Visits>2</Number_of_Visits>

                <Continuous_Visits>false</Continuous_Visits>
                
                <Priority>1</Priority>


                    <!-- This parameter defines the minimum on-source time relative to the visit duration                                                 -->
                    <!--       (excluding interruptions due to the SAA, Earth Occultations, and straylight constraints)                                   -->
                    <!-- NOTE: For visits with scheduling flexibility, especially those shorter than 3 orbits, the effective                              -->
                    <!--       observing efficiency may end up to be lower than the requested value by up to ~ 15%.                                       -->
                    <!--       This may happen under special circumstances, typically when the scheduleSolver algorithm adjusts                           -->
                    <!--       the visit start time to optimise the overhall schedule, which may result in a visit being shifted                          -->
                    <!--       toward the SAA, Earth occultations or straylight regions.                                                                  -->
                <!--  #############################   Set the minimum effective duration, ~ observing efficiency (in percent)                             -->
                <Minimum_Effective_Duration unit="%">50</Minimum_Effective_Duration>

                <!--  You are done with editing the observation request file. It can now be ingested in the FC.    -->
				<Send_Data_Taking_During_SAA>false</Send_Data_Taking_During_SAA>
				<Send_Data_Taking_During_Earth_Constraints>false</Send_Data_Taking_During_Earth_Constraints>
				<PITL>true</PITL>
			</Non_Time_Critical_Request>
    </List_of_Non_Time_Critical_Requests>
  </Data_Block>
</Earth_Explorer_File>
