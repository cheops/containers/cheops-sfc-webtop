--
-- PostgreSQL database dump
--

-- Dumped from database version 16.7
-- Dumped by pg_dump version 16.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: delete_empty_linkedevents(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_empty_linkedevents() RETURNS character varying[]
    LANGUAGE plpgsql
    AS $$
DECLARE
   linkedevents_to_delete varchar[];
BEGIN
		 
   linkedevents_to_delete := ARRAY(
	   SELECT entity_id
	   FROM linked_events
	   WHERE entity_id NOT IN (SELECT distinct(linkedevents) FROM single_event WHERE linkedevents IS NOT NULL));
		 
   IF array_length(linkedevents_to_delete, 1) > 0 THEN
   
     RAISE NOTICE '% empty linked events are going to be deleted', array_length(linkedevents_to_delete, 1);
   
     DELETE FROM linked_events WHERE entity_id = ANY(linkedevents_to_delete);
     DELETE FROM occurrence WHERE entity_id = ANY(linkedevents_to_delete);
   END IF;
		 
   RETURN linkedevents_to_delete;
   
END;
$$;


ALTER FUNCTION public.delete_empty_linkedevents() OWNER TO postgres;

--
-- Name: delete_linkedevents(character varying[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_linkedevents(linkedevents_to_delete character varying[]) RETURNS character varying[]
    LANGUAGE plpgsql
    AS $$
DECLARE
   linkedevents_with_visits varchar[];
   visits_to_delete varchar[];
   deleted_visits varchar[];
   deleted_linked_events varchar[];
BEGIN
   
   -- Select linked events with visits (visits need to be deleted first)
   IF (SELECT NOT EXISTS(SELECT 1 FROM information_schema.views WHERE table_name = 'visits_in_linked_events')) THEN
     CREATE OR REPLACE VIEW visits_in_linked_events
     as
     SELECT visit.entity_id, linkedevents FROM visit INNER JOIN single_event ON visit.entity_id = single_event.entity_id
     WHERE linkedevents IS NOT NULL;
   END IF;
   
   linkedevents_with_visits := ARRAY(
		SELECT DISTINCT(linkedevents) FROM visits_in_linked_events WHERE linkedevents = ANY(linkedevents_to_delete));
		 
   visits_to_delete := ARRAY(
		SELECT entity_id FROM visits_in_linked_events WHERE linkedevents = ANY(linkedevents_with_visits));
		 
   IF array_length(visits_to_delete, 1) > 0 THEN
   	 deleted_visits := delete_visits(visits_to_delete, false);
	 RAISE NOTICE '% visits were deleted by linked_events', array_length(deleted_visits, 1);
   END IF;
   
   -- Delete linked events
   DELETE FROM linked_events WHERE entity_id = ANY(linkedevents_to_delete);
   DELETE FROM occurrence WHERE entity_id = ANY(linkedevents_to_delete); 
		 
   RETURN deleted_linked_events || deleted_visits;
   
END;
$$;


ALTER FUNCTION public.delete_linkedevents(linkedevents_to_delete character varying[]) OWNER TO postgres;

--
-- Name: delete_occurrences(character varying[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_occurrences(occurrences_to_delete character varying[]) RETURNS character varying[]
    LANGUAGE plpgsql
    AS $$
DECLARE
   visits_to_delete varchar[];
   unavailabilities_to_delete varchar[];
   linkedevents_to_delete varchar[];
   activities_to_delete varchar[];
   deleted_visits varchar[];
   deleted_unavailabilities varchar[];
   deleted_linkedevents varchar[];
BEGIN

   -- Delete Visits
   visits_to_delete := ARRAY(
	   SELECT entity_id FROM visit WHERE entity_id = ANY(occurrences_to_delete));
	   
   IF array_length(visits_to_delete, 1) > 0 THEN
     deleted_visits := delete_visits(visits_to_delete, false);
     RAISE NOTICE 'Deleted % visits', array_length(deleted_visits, 1);
   END IF;
   
   -- Delete Unavailabilities
   unavailabilities_to_delete := ARRAY(
	   SELECT entity_id FROM unavailability WHERE entity_id = ANY(occurrences_to_delete));
   
   IF array_length(unavailabilities_to_delete, 1) > 0 THEN
   
     -- Delete Activities
     activities_to_delete := ARRAY(
	   SELECT entity_id FROM activity WHERE occurrence = ANY(unavailabilities_to_delete));
     DELETE FROM platform_unavailability WHERE entity_id = ANY(activities_to_delete);
     DELETE FROM activity WHERE entity_id = ANY(activities_to_delete);
     DELETE FROM occurrence_plan_activities WHERE activity_id = ANY(activities_to_delete);
   
     -- Delete unavailability
     DELETE FROM unavailability WHERE entity_id = ANY(unavailabilities_to_delete);
     DELETE FROM single_event WHERE entity_id = ANY(unavailabilities_to_delete);
     DELETE FROM occurrence WHERE entity_id = ANY(unavailabilities_to_delete);
     deleted_unavailabilities := unavailabilities_to_delete;
     RAISE NOTICE 'Deleted % unavailabilities', array_length(unavailabilities_to_delete, 1);
   END IF;
   
      
   -- Delete Linked Events (this procedure deletes empty envelopes)
   linkedevents_to_delete := ARRAY(
	   SELECT entity_id FROM linked_events WHERE entity_id = ANY(occurrences_to_delete));
   
   IF array_length(linkedevents_to_delete, 1) > 0 THEN
     deleted_linkedevents := delete_linkedevents(linkedevents_to_delete);
     RAISE NOTICE 'Deleted % linked events', array_length(linkedevents_to_delete, 1);
   END IF;
   
   RETURN deleted_visits || deleted_unavailabilities || deleted_linkedevents;
END;
$$;


ALTER FUNCTION public.delete_occurrences(occurrences_to_delete character varying[]) OWNER TO postgres;

--
-- Name: delete_visits(character varying[], boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_visits(visits_to_delete character varying[], delete_empty_envelopes boolean) RETURNS character varying[]
    LANGUAGE plpgsql
    AS $$
DECLARE
   activities_to_delete varchar[];
   profiles_to_delete varchar[];
   measures_to_delete varchar[];
   deleted_envelopes varchar[];
BEGIN
   
   -- Delete Activities
   activities_to_delete := ARRAY(
	   SELECT entity_id FROM activity WHERE occurrence = ANY(visits_to_delete));
   
   -- Configuration Dynamic
   DELETE FROM dynamic_parameter WHERE configurationdynamicactivity = ANY(activities_to_delete);
   DELETE FROM configuration_dynamic WHERE entity_id = ANY(activities_to_delete);
   -- Nominal observation
   DELETE FROM nominal_observation WHERE entity_id = ANY(activities_to_delete);
   -- Star Map
   DELETE FROM star_map WHERE entity_id = ANY(activities_to_delete);
   -- Start/Stop Science Data Suspend
   DELETE FROM start_science_data_suspend WHERE entity_id = ANY(activities_to_delete);
   DELETE FROM stop_science_data_suspend WHERE entity_id = ANY(activities_to_delete);
   -- Finally, DELETE FROM activity table and from occurrence_plan_activities relationship
   DELETE FROM activity WHERE entity_id = ANY(activities_to_delete);
   DELETE FROM occurrence_plan_activities WHERE activity_id = ANY(activities_to_delete);
   	   
   -- Visit
   DELETE FROM visit_status WHERE visit = ANY(visits_to_delete);
   DELETE FROM phase_range_effectiveness WHERE entity_id = ANY(visits_to_delete);
   DELETE FROM visit WHERE entity_id = ANY(visits_to_delete);
   
   -- Single_event
   DELETE FROM single_event WHERE entity_id = ANY(visits_to_delete);
   
   -- Occurrence
   profiles_to_delete := ARRAY(
	   SELECT entity_id FROM occurrence_profile WHERE occurrence = ANY(visits_to_delete));
   measures_to_delete := ARRAY(
	   SELECT entity_id FROM measure WHERE resourceprofile = ANY(profiles_to_delete));
	   
   -- Delete measures
   DELETE FROM interval_measure WHERE entity_id = ANY(measures_to_delete);
   DELETE FROM boolean_measure WHERE entity_id = ANY(measures_to_delete);
   DELETE FROM double_measure WHERE entity_id = ANY(measures_to_delete);
   DELETE FROM measure WHERE entity_id = ANY(measures_to_delete);
   
   -- Delete profiles
   DELETE FROM occurrence_profile WHERE entity_id = ANY(profiles_to_delete);
   DELETE FROM resource_profile WHERE entity_id = ANY(profiles_to_delete);
   
   DELETE FROM occurrence WHERE entity_id = ANY(visits_to_delete);
   
   -- Finally, delete empty envelopes (if requested)
   IF delete_empty_envelopes THEN
     deleted_envelopes := delete_empty_linkedevents();
   END IF;
   
   RETURN visits_to_delete || deleted_envelopes;
END;
$$;


ALTER FUNCTION public.delete_visits(visits_to_delete character varying[], delete_empty_envelopes boolean) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: activity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activity (
    entity_id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    external_id character varying(255) NOT NULL,
    validity_status integer NOT NULL,
    activity_index integer,
    execution_status integer NOT NULL,
    name character varying(255),
    planning_status integer NOT NULL,
    start timestamp without time zone,
    stop timestamp without time zone,
    occurrence character varying(255)
);


ALTER TABLE public.activity OWNER TO postgres;

--
-- Name: activity_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activity_audit_event (
    activity_entity_id character varying(255) NOT NULL,
    events_audit_event_id character varying(255) NOT NULL
);


ALTER TABLE public.activity_audit_event OWNER TO postgres;

--
-- Name: algorithms_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.algorithms_data (
    entity_id character varying(255) NOT NULL,
    conversion_aocs_cyles_to_seconds double precision NOT NULL,
    tc_max_activity_plan_size integer NOT NULL,
    mem_default_recording_rate double precision NOT NULL,
    mem_downlink_rate double precision NOT NULL,
    mem_downlink_rate_tm double precision NOT NULL,
    mem_capacity_science_ps double precision NOT NULL,
    mem_mps_error_threshold double precision NOT NULL,
    mem_mps_warn_threshold double precision NOT NULL,
    slew_delta_t_aocs double precision NOT NULL,
    slew_i_sat_row0_col0 double precision NOT NULL,
    slew_i_sat_row0_col1 double precision NOT NULL,
    slew_i_sat_row0_col2 double precision NOT NULL,
    slew_i_sat_row1_col0 double precision NOT NULL,
    slew_i_sat_row1_col1 double precision NOT NULL,
    slew_i_sat_row1_col2 double precision NOT NULL,
    slew_i_sat_row2_col0 double precision NOT NULL,
    slew_i_sat_row2_col1 double precision NOT NULL,
    slew_i_sat_row2_col2 double precision NOT NULL,
    slew_reaction_wheel_1_status integer NOT NULL,
    slew_reaction_wheel_2_status integer NOT NULL,
    slew_reaction_wheel_3_status integer NOT NULL,
    slew_reaction_wheel_4_status integer NOT NULL,
    slew_yaw_tilt_angle double precision NOT NULL,
    algorithms_file character varying(255) NOT NULL
);


ALTER TABLE public.algorithms_data OWNER TO postgres;

--
-- Name: algorithms_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.algorithms_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.algorithms_file OWNER TO postgres;

--
-- Name: audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.audit_event (
    audit_event_id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    description character varying(255),
    is_manual boolean NOT NULL,
    time_stamp bigint NOT NULL,
    type integer NOT NULL,
    source_file_id character varying(255)
);


ALTER TABLE public.audit_event OWNER TO postgres;

--
-- Name: binary_resource; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.binary_resource (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.binary_resource OWNER TO postgres;

--
-- Name: boolean_measure; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.boolean_measure (
    time_stamp timestamp without time zone NOT NULL,
    value boolean NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.boolean_measure OWNER TO postgres;

--
-- Name: configuration_dynamic; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.configuration_dynamic (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.configuration_dynamic OWNER TO postgres;

--
-- Name: double_measure; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.double_measure (
    time_stamp timestamp without time zone NOT NULL,
    value double precision NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.double_measure OWNER TO postgres;

--
-- Name: downlink; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.downlink (
    entity_id character varying(255) NOT NULL,
    gs_availability character varying(255)
);


ALTER TABLE public.downlink OWNER TO postgres;

--
-- Name: dynamic_parameter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dynamic_parameter (
    entity_id character varying(255) NOT NULL,
    parameter_id character varying(255) NOT NULL,
    parameter_value character varying(255) NOT NULL,
    configurationdynamicactivity character varying(255)
);


ALTER TABLE public.dynamic_parameter OWNER TO postgres;

--
-- Name: external_id_generator; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.external_id_generator (
    entity_id character varying(255) NOT NULL,
    external_id_counter integer NOT NULL
);


ALTER TABLE public.external_id_generator OWNER TO postgres;

--
-- Name: fbf_transfer_disabled_fbfs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fbf_transfer_disabled_fbfs (
    entity_id character varying(255) NOT NULL,
    disabled_fbfs integer NOT NULL
);


ALTER TABLE public.fbf_transfer_disabled_fbfs OWNER TO postgres;

--
-- Name: fbftransfer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fbftransfer (
    fbf_end bigint NOT NULL,
    fbf_init bigint NOT NULL,
    fbf_size bigint NOT NULL,
    is_generated_manually boolean NOT NULL,
    number_of_fbfs bigint NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.fbftransfer OWNER TO postgres;

--
-- Name: flash_based_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.flash_based_file (
    entity_id character varying(255) NOT NULL,
    corrupted boolean NOT NULL,
    disabled boolean NOT NULL,
    download boolean NOT NULL,
    logical_address integer NOT NULL,
    number_of_writes integer NOT NULL,
    written_at timestamp without time zone,
    activity character varying(255)
);


ALTER TABLE public.flash_based_file OWNER TO postgres;

--
-- Name: gen_baseline_src_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gen_baseline_src_file (
    generation_baseline character varying(255) NOT NULL,
    source_file character varying(255) NOT NULL
);


ALTER TABLE public.gen_baseline_src_file OWNER TO postgres;

--
-- Name: generation_baseline; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.generation_baseline (
    entity_id character varying(255) NOT NULL,
    generation_period_end timestamp without time zone,
    generation_period_start timestamp without time zone
);


ALTER TABLE public.generation_baseline OWNER TO postgres;

--
-- Name: geometric_resource; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.geometric_resource (
    maximumvalue double precision NOT NULL,
    minimumvalue double precision NOT NULL,
    threshold double precision,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.geometric_resource OWNER TO postgres;

--
-- Name: gsavailability; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gsavailability (
    entity_id character varying(255) NOT NULL,
    duration bigint NOT NULL,
    ground_station integer NOT NULL,
    pass_id character varying(255),
    start_time timestamp without time zone NOT NULL,
    gsavailability_file character varying(255) NOT NULL
);


ALTER TABLE public.gsavailability OWNER TO postgres;

--
-- Name: gsavailability_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gsavailability_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.gsavailability_file OWNER TO postgres;

--
-- Name: hktmparameters; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hktmparameters (
    entity_id character varying(255) NOT NULL,
    filled_space double precision,
    free_space bigint,
    "timestamp" timestamp without time zone NOT NULL,
    used_space bigint,
    hktmparameters_file character varying(255) NOT NULL
);


ALTER TABLE public.hktmparameters OWNER TO postgres;

--
-- Name: hktmparameters_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hktmparameters_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.hktmparameters_file OWNER TO postgres;

--
-- Name: idle_target; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.idle_target (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.idle_target OWNER TO postgres;

--
-- Name: interval_measure; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.interval_measure (
    start timestamp without time zone NOT NULL,
    stop timestamp without time zone NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.interval_measure OWNER TO postgres;

--
-- Name: interval_resource; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.interval_resource (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.interval_resource OWNER TO postgres;

--
-- Name: leapsecond_value; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.leapsecond_value (
    entity_id character varying(255) NOT NULL,
    date timestamp without time zone NOT NULL,
    seconds integer NOT NULL,
    leapseconds_file character varying(255) NOT NULL
);


ALTER TABLE public.leapsecond_value OWNER TO postgres;

--
-- Name: leapseconds_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.leapseconds_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.leapseconds_file OWNER TO postgres;

--
-- Name: linked_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.linked_events (
    scheduling_block boolean NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.linked_events OWNER TO postgres;

--
-- Name: master_baseline; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.master_baseline (
    entity_id character varying(255) NOT NULL,
    generation_baseline_id character varying(255) NOT NULL
);


ALTER TABLE public.master_baseline OWNER TO postgres;

--
-- Name: master_profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.master_profile (
    end_of_computed_interval timestamp without time zone,
    start_of_computed_interval timestamp without time zone,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.master_profile OWNER TO postgres;

--
-- Name: measure; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measure (
    type character varying(31) NOT NULL,
    entity_id character varying(255) NOT NULL,
    resourceprofile character varying(255)
);


ALTER TABLE public.measure OWNER TO postgres;

--
-- Name: nominal_observation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nominal_observation (
    acq_flag bigint NOT NULL,
    centroidselection bigint,
    exp_time_acq bigint,
    image_req_acq bigint,
    numberofimages bigint,
    stackingorder bigint,
    cal1_flag bigint NOT NULL,
    cal2_flag bigint NOT NULL,
    ccd_readout_mode_sci bigint NOT NULL,
    cib_full_number bigint,
    cib_full_size bigint,
    cib_window_number bigint,
    cib_window_size bigint,
    first_fbf bigint NOT NULL,
    last_fbf bigint NOT NULL,
    centroid_flag_cal1 bigint,
    exp_time_cal1 bigint,
    image_req_cal1 bigint,
    nr_of_images_cal1 bigint,
    stacking_order_cal1 bigint,
    gib_full_number bigint,
    gib_full_size bigint,
    gib_window_number bigint,
    gib_window_size bigint,
    save_target bigint NOT NULL,
    sci_flag bigint NOT NULL,
    centroid_flag_sci bigint,
    exp_time_sci bigint,
    image_req_sci bigint,
    nr_of_images_sci bigint,
    stacking_order_sci bigint,
    centroid_flag_cal2 bigint,
    exp_time_cal2 bigint,
    image_req_cal2 bigint,
    nr_of_images_cal2 bigint,
    stacking_order_cal2 bigint,
    sib_full_number bigint,
    sib_full_size bigint,
    sib_window_number bigint,
    sib_window_size bigint,
    win_pos_x_sci bigint,
    win_pos_y_sci bigint,
    win_size_x_sci bigint,
    win_size_y_sci bigint,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.nominal_observation OWNER TO postgres;

--
-- Name: observation_id; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.observation_id (
    entity_id character varying(255) NOT NULL,
    observation_id character varying(255) NOT NULL
);


ALTER TABLE public.observation_id OWNER TO postgres;

--
-- Name: observation_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.observation_request (
    avoid_earth_occultation boolean,
    central_time double precision,
    earliest_observation_start double precision,
    earliest_start bigint,
    expected_visit_duration double precision,
    external_observation_request_id character varying(255) NOT NULL,
    fulfil_all_phase_ranges boolean,
    latest_end bigint,
    latest_observation_start double precision,
    margin_mode integer,
    minimum_effective_duration double precision,
    minimum_number_of_visits integer NOT NULL,
    number_stacked_imagettes integer NOT NULL,
    observation_repeat_period double precision,
    pitl boolean NOT NULL,
    proprietary_period_first_visit integer NOT NULL,
    proprietary_period_last_visit integer NOT NULL,
    respect_moon_exclusion_angle boolean,
    schedule_continuously boolean NOT NULL,
    send_data_taking_during_earth_constraints boolean,
    send_data_taking_during_saa boolean,
    stack integer NOT NULL,
    window_offset_x integer,
    window_offset_y integer,
    window_size_x integer,
    window_size_y integer,
    window_type integer,
    entity_id character varying(255) NOT NULL,
    sourcefile_entity_id character varying(255)
);


ALTER TABLE public.observation_request OWNER TO postgres;

--
-- Name: occurrence; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occurrence (
    entity_id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    external_id character varying(255) NOT NULL,
    validity_status integer NOT NULL,
    execution_status integer NOT NULL,
    is_locked boolean,
    planning_status integer NOT NULL,
    slot_start timestamp without time zone NOT NULL,
    slot_stop timestamp without time zone NOT NULL,
    generation_baseline character varying(255)
);


ALTER TABLE public.occurrence OWNER TO postgres;

--
-- Name: occurrence_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occurrence_audit_event (
    occurrence_entity_id character varying(255) NOT NULL,
    events_audit_event_id character varying(255) NOT NULL
);


ALTER TABLE public.occurrence_audit_event OWNER TO postgres;

--
-- Name: occurrence_plan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occurrence_plan (
    entity_id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    external_id character varying(255) NOT NULL,
    validity_status integer NOT NULL,
    coverage_start timestamp without time zone NOT NULL,
    coverage_stop timestamp without time zone NOT NULL,
    execution_status integer NOT NULL,
    filename character varying(255),
    uplinked boolean NOT NULL,
    generation_baseline character varying(255)
);


ALTER TABLE public.occurrence_plan OWNER TO postgres;

--
-- Name: occurrence_plan_activities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occurrence_plan_activities (
    occurrence_plan_id character varying(255) NOT NULL,
    activity_id character varying(255) NOT NULL,
    activities_index integer NOT NULL
);


ALTER TABLE public.occurrence_plan_activities OWNER TO postgres;

--
-- Name: occurrence_plan_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occurrence_plan_audit_event (
    occurrence_plan_entity_id character varying(255) NOT NULL,
    events_audit_event_id character varying(255) NOT NULL
);


ALTER TABLE public.occurrence_plan_audit_event OWNER TO postgres;

--
-- Name: occurrence_profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occurrence_profile (
    entity_id character varying(255) NOT NULL,
    occurrence character varying(255)
);


ALTER TABLE public.occurrence_profile OWNER TO postgres;

--
-- Name: orbit_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orbit_file (
    center_name character varying(255),
    interpolation character varying(255),
    interpolation_degree numeric(19,2),
    object_id character varying(255),
    object_name character varying(255),
    ref_frame character varying(255),
    time_system character varying(255),
    usable_start_time timestamp without time zone,
    usable_stop_time timestamp without time zone,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.orbit_file OWNER TO postgres;

--
-- Name: parameters_activity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_activity (
    entity_id character varying(255) NOT NULL,
    tx_identifier_start integer,
    tx_identifier_stop integer,
    fbf_size_default_value smallint NOT NULL,
    base_win_image_cekey bigint NOT NULL,
    base_win_image_psf_flat_field_cekey bigint NOT NULL,
    base_win_imgtt_cekey bigint NOT NULL,
    circle_win_image_cekey bigint NOT NULL,
    circle_win_imgtt_cekey bigint NOT NULL,
    disable_win_imgtt_cekey bigint NOT NULL,
    lossy1_win_image_cekey bigint NOT NULL,
    lossy1_win_imgtt_cekey bigint NOT NULL,
    nlc_mlblk_cekey bigint NOT NULL,
    nlc_mldk_cekey bigint NOT NULL,
    nlc_mlos_cekey bigint NOT NULL,
    nlc_mrblk_cekey bigint NOT NULL,
    nlc_mrdk_cekey bigint NOT NULL,
    nlc_mtdk_cekey bigint NOT NULL,
    nlc_mtos_cekey bigint NOT NULL
);


ALTER TABLE public.parameters_activity OWNER TO postgres;

--
-- Name: parameters_ce_visitconfdynamic; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_ce_visitconfdynamic (
    entity_id character varying(255) NOT NULL,
    alias character varying(255) NOT NULL,
    array_element integer NOT NULL,
    description character varying(255) NOT NULL,
    on_board_id character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    dynamic_ce_parameters character varying(255)
);


ALTER TABLE public.parameters_ce_visitconfdynamic OWNER TO postgres;

--
-- Name: parameters_ce_visitconfdynamic_value_per_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_ce_visitconfdynamic_value_per_type (
    visitconfigurationdynamicceparameter_entity_id character varying(255) NOT NULL,
    valuespercategory character varying(255),
    ccd_pos_order integer NOT NULL,
    margin_mode integer NOT NULL,
    observation_category integer NOT NULL,
    visit_order integer NOT NULL,
    window_type integer NOT NULL
);


ALTER TABLE public.parameters_ce_visitconfdynamic_value_per_type OWNER TO postgres;

--
-- Name: parameters_corrupted_fbfs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_corrupted_fbfs (
    mpsdefaultsdata_entity_id character varying(255) NOT NULL,
    fbf_logical_address integer NOT NULL
);


ALTER TABLE public.parameters_corrupted_fbfs OWNER TO postgres;

--
-- Name: parameters_defaults_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_defaults_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.parameters_defaults_file OWNER TO postgres;

--
-- Name: parameters_fbf_size_per_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_fbf_size_per_type (
    activityparameters_entity_id character varying(255) NOT NULL,
    fbfsizevaluespercategory smallint,
    observation_category integer NOT NULL
);


ALTER TABLE public.parameters_fbf_size_per_type OWNER TO postgres;

--
-- Name: parameters_general; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_general (
    entity_id character varying(255) NOT NULL,
    data_downlink_offset integer DEFAULT 0 NOT NULL,
    default_earth_limb_altitude double precision DEFAULT 0 NOT NULL,
    min_moon_angle double precision NOT NULL,
    saa_data_suspend_offset integer NOT NULL
);


ALTER TABLE public.parameters_general OWNER TO postgres;

--
-- Name: parameters_general_earth_limb_altitude_per_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_general_earth_limb_altitude_per_category (
    generalparameters_entity_id character varying(255) NOT NULL,
    earthlimbaltitudepercategory double precision,
    category integer NOT NULL
);


ALTER TABLE public.parameters_general_earth_limb_altitude_per_category OWNER TO postgres;

--
-- Name: parameters_general_max_straylight_per_mag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_general_max_straylight_per_mag (
    generalparameters_entity_id character varying(255) NOT NULL,
    maxstraylightfluxpermag double precision,
    mag double precision NOT NULL
);


ALTER TABLE public.parameters_general_max_straylight_per_mag OWNER TO postgres;

--
-- Name: parameters_general_max_straylight_per_mc_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_general_max_straylight_per_mc_category (
    generalparameters_entity_id character varying(255) NOT NULL,
    maxstraylightfluxpermccategory double precision,
    category integer NOT NULL
);


ALTER TABLE public.parameters_general_max_straylight_per_mc_category OWNER TO postgres;

--
-- Name: parameters_general_programme_time_allocation_period; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_general_programme_time_allocation_period (
    generalparameters_entity_id character varying(255) NOT NULL,
    period_end timestamp without time zone NOT NULL,
    max_ddt double precision NOT NULL,
    slack double precision NOT NULL,
    period_start timestamp without time zone NOT NULL,
    time_goddt double precision NOT NULL,
    time_gto double precision NOT NULL
);


ALTER TABLE public.parameters_general_programme_time_allocation_period OWNER TO postgres;

--
-- Name: parameters_mps_defaults; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_mps_defaults (
    entity_id character varying(255) NOT NULL,
    imagettes_extraction_strategy character varying(255),
    imagettes_shape integer,
    imagettes_size_x integer,
    imagettes_size_y integer,
    margin_dark_left_mask integer,
    margin_dark_right_mask integer,
    margin_mode integer,
    window_offset_x integer,
    window_offset_x_per_order character varying(1020),
    window_offset_y integer,
    window_offset_y_per_order character varying(1020),
    window_shape integer,
    window_size_x integer,
    window_size_y integer,
    acqfulldropt1 integer NOT NULL,
    acqfulldropt2 integer NOT NULL,
    calfullsnapt1 integer NOT NULL,
    calfullsnapt2 integer NOT NULL,
    ce_overhead integer NOT NULL,
    exposure_time_acq_limit integer NOT NULL,
    fbf_first integer NOT NULL,
    fbf_last integer NOT NULL,
    fbf_max_transfer integer NOT NULL,
    fbf_max_write_operations integer NOT NULL,
    fbf_size integer NOT NULL,
    fbf_transfer_suspend integer NOT NULL,
    fbf_transfer_time integer NOT NULL,
    full_repetition_limit integer NOT NULL,
    instrument_hk_recording_rate integer NOT NULL,
    imagettes_limit integer NOT NULL,
    max_num_image_acq integer NOT NULL,
    move_time_dark_off integer NOT NULL,
    move_time_first integer NOT NULL,
    move_time_following integer NOT NULL,
    multi_full_repetition_period integer NOT NULL,
    repetition_margin integer NOT NULL,
    sciwinstackt1 integer NOT NULL,
    sciwinstackt2 integer NOT NULL,
    t_flush integer NOT NULL,
    time_to_science integer DEFAULT 0 NOT NULL,
    time_to_stabilize integer DEFAULT 0 NOT NULL,
    algorithm_id_pitl_false integer NOT NULL,
    algorithm_id_pitl_true integer NOT NULL,
    distance_threshold integer NOT NULL,
    iterations integer NOT NULL,
    pointing_uncertainty integer NOT NULL,
    activity_parameters character varying(255) NOT NULL,
    general_parameters character varying(255) NOT NULL,
    source_file character varying(255) NOT NULL
);


ALTER TABLE public.parameters_mps_defaults OWNER TO postgres;

--
-- Name: parameters_obs_category_parameters_per_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_obs_category_parameters_per_type (
    mpsdefaultsdata_entity_id character varying(255) NOT NULL,
    imagettes_extraction_strategy character varying(255),
    imagettes_shape integer,
    imagettes_size_x integer,
    imagettes_size_y integer,
    margin_dark_left_mask integer,
    margin_dark_right_mask integer,
    margin_mode integer,
    window_offset_x integer,
    window_offset_x_per_order character varying(1020),
    window_offset_y integer,
    window_offset_y_per_order character varying(1020),
    window_shape integer,
    window_size_x integer,
    window_size_y integer,
    observation_category integer NOT NULL
);


ALTER TABLE public.parameters_obs_category_parameters_per_type OWNER TO postgres;

--
-- Name: parameters_visitacq; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_visitacq (
    entity_id character varying(255) NOT NULL,
    default_value character varying(255),
    parameter_name character varying(255) NOT NULL,
    value_type character varying(255) NOT NULL,
    visit_acquisition_parameters character varying(255)
);


ALTER TABLE public.parameters_visitacq OWNER TO postgres;

--
-- Name: parameters_visitacq_value_per_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_visitacq_value_per_type (
    visitacquisitionparameter_entity_id character varying(255) NOT NULL,
    valuespercategory character varying(255),
    acquisition_type integer NOT NULL,
    observation_category integer NOT NULL,
    window_type integer NOT NULL
);


ALTER TABLE public.parameters_visitacq_value_per_type OWNER TO postgres;

--
-- Name: parameters_visitconfdynamic; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_visitconfdynamic (
    entity_id character varying(255) NOT NULL,
    alias character varying(255) NOT NULL,
    array_element integer NOT NULL,
    description character varying(255) NOT NULL,
    on_board_id character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    dynamic_parameters character varying(255)
);


ALTER TABLE public.parameters_visitconfdynamic OWNER TO postgres;

--
-- Name: parameters_visitconfdynamic_value_per_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parameters_visitconfdynamic_value_per_type (
    visitconfigurationdynamicparameter_entity_id character varying(255) NOT NULL,
    valuespercategory character varying(255),
    ccd_pos_order integer NOT NULL,
    observation_category integer NOT NULL,
    visit_order integer NOT NULL,
    window_type integer NOT NULL
);


ALTER TABLE public.parameters_visitconfdynamic_value_per_type OWNER TO postgres;

--
-- Name: payload_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payload_request (
    entity_id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    external_id character varying(255) NOT NULL,
    validity_status integer NOT NULL,
    planning_status integer NOT NULL,
    priority integer,
    reason character varying(255),
    sub_type integer NOT NULL,
    program character varying(255)
);


ALTER TABLE public.payload_request OWNER TO postgres;

--
-- Name: payload_request_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payload_request_audit_event (
    payload_request_entity_id character varying(255) NOT NULL,
    events_audit_event_id character varying(255) NOT NULL
);


ALTER TABLE public.payload_request_audit_event OWNER TO postgres;

--
-- Name: phase_range; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.phase_range (
    entity_id character varying(255) NOT NULL,
    phase_range_min_effective_visit double precision,
    phase_range_start double precision,
    phase_range_stop double precision,
    request_id character varying(255)
);


ALTER TABLE public.phase_range OWNER TO postgres;

--
-- Name: phase_range_effectiveness; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.phase_range_effectiveness (
    entity_id character varying(255) NOT NULL,
    computed_effectiveness double precision NOT NULL,
    phase_range character varying(255),
    real_effectiveness double precision,
    start timestamp without time zone NOT NULL,
    stop timestamp without time zone NOT NULL
);


ALTER TABLE public.phase_range_effectiveness OWNER TO postgres;

--
-- Name: planning_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.planning_file (
    context integer NOT NULL,
    placeholder boolean NOT NULL,
    processing_status integer NOT NULL,
    processing_timestamp timestamp without time zone,
    product_type integer NOT NULL,
    validation_status integer NOT NULL,
    validity_start timestamp without time zone,
    validity_stop timestamp without time zone,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.planning_file OWNER TO postgres;

--
-- Name: planning_resource; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.planning_resource (
    type character varying(31) NOT NULL,
    entity_id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    end_of_applicable_time timestamp without time zone,
    name integer NOT NULL,
    start_of_applicable_time timestamp without time zone,
    measure_type integer NOT NULL
);


ALTER TABLE public.planning_resource OWNER TO postgres;

--
-- Name: planning_resource_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.planning_resource_audit_event (
    planning_resource_entity_id character varying(255) NOT NULL,
    events_audit_event_id character varying(255) NOT NULL
);


ALTER TABLE public.planning_resource_audit_event OWNER TO postgres;

--
-- Name: platform_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.platform_request (
    entity_id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    external_id character varying(255) NOT NULL,
    validity_status integer NOT NULL,
    planning_status integer NOT NULL,
    priority integer,
    reason character varying(255)
);


ALTER TABLE public.platform_request OWNER TO postgres;

--
-- Name: platform_request_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.platform_request_audit_event (
    platform_request_entity_id character varying(255) NOT NULL,
    events_audit_event_id character varying(255) NOT NULL
);


ALTER TABLE public.platform_request_audit_event OWNER TO postgres;

--
-- Name: platform_unavailability; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.platform_unavailability (
    request_id bigint NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.platform_unavailability OWNER TO postgres;

--
-- Name: program; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.program (
    entity_id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    external_id character varying(255) NOT NULL,
    validity_status integer NOT NULL,
    aocycle integer NOT NULL,
    external_program_id character varying(255) NOT NULL,
    pi_affiliation character varying(255) NOT NULL,
    pi_email character varying(255) NOT NULL,
    pi_name character varying(255) NOT NULL,
    pi_uid integer NOT NULL,
    proposal_abstract character varying(3000),
    secondary_email character varying(255),
    secondary_name character varying(255),
    title character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.program OWNER TO postgres;

--
-- Name: program_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.program_audit_event (
    program_entity_id character varying(255) NOT NULL,
    events_audit_event_id character varying(255) NOT NULL
);


ALTER TABLE public.program_audit_event OWNER TO postgres;

--
-- Name: pst_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pst_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.pst_file OWNER TO postgres;

--
-- Name: pst_value; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pst_value (
    entity_id character varying(255) NOT NULL,
    angle double precision NOT NULL,
    value double precision NOT NULL,
    pst_file character varying(255) NOT NULL
);


ALTER TABLE public.pst_value OWNER TO postgres;

--
-- Name: request_exposure_time; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request_exposure_time (
    entity_id character varying(255) NOT NULL,
    exposure_time double precision NOT NULL,
    listofexposuretimes_order integer NOT NULL
);


ALTER TABLE public.request_exposure_time OWNER TO postgres;

--
-- Name: request_number_of_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request_number_of_images (
    entity_id character varying(255) NOT NULL,
    number_of_images integer
);


ALTER TABLE public.request_number_of_images OWNER TO postgres;

--
-- Name: request_readout_mode; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request_readout_mode (
    entity_id character varying(255) NOT NULL,
    readout_mode integer NOT NULL,
    readoutmodes_order integer NOT NULL
);


ALTER TABLE public.request_readout_mode OWNER TO postgres;

--
-- Name: request_repetition_period; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request_repetition_period (
    entity_id character varying(255) NOT NULL,
    repetition_period double precision,
    listofrepetitionperiods_order integer NOT NULL
);


ALTER TABLE public.request_repetition_period OWNER TO postgres;

--
-- Name: request_target; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request_target (
    entity_id character varying(255) NOT NULL,
    declination double precision NOT NULL,
    effective_temperature integer,
    extinction double precision,
    gaia_id character varying(255),
    is_catalogued_star boolean,
    magnitude double precision,
    magnitude_error double precision,
    parallax double precision,
    proper_motion_declination double precision,
    proper_motion_right_ascension double precision,
    right_ascension double precision NOT NULL,
    spectral_type character varying(255),
    target_name character varying(255) NOT NULL,
    requesttargets_order integer NOT NULL
);


ALTER TABLE public.request_target OWNER TO postgres;

--
-- Name: request_visit_offset; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request_visit_offset (
    entity_id character varying(255) NOT NULL,
    ccd_pos_order integer NOT NULL,
    visit_offset_x integer NOT NULL,
    visit_offset_y integer NOT NULL,
    visit_order integer NOT NULL,
    visitoffsets_order integer NOT NULL
);


ALTER TABLE public.request_visit_offset OWNER TO postgres;

--
-- Name: resource_profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_profile (
    entity_id character varying(255) NOT NULL,
    resource character varying(255)
);


ALTER TABLE public.resource_profile OWNER TO postgres;

--
-- Name: saa_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.saa_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.saa_file OWNER TO postgres;

--
-- Name: saa_safe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.saa_safe (
    saa_counter bigint NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.saa_safe OWNER TO postgres;

--
-- Name: saa_value; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.saa_value (
    entity_id character varying(255) NOT NULL,
    is_affected boolean NOT NULL,
    latitude integer NOT NULL,
    longitude integer NOT NULL,
    saa_file character varying(255) NOT NULL
);


ALTER TABLE public.saa_value OWNER TO postgres;

--
-- Name: safe_acquisition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.safe_acquisition (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.safe_acquisition OWNER TO postgres;

--
-- Name: single_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.single_event (
    flexibility_window_start timestamp without time zone,
    flexibility_window_stop timestamp without time zone,
    entity_id character varying(255) NOT NULL,
    linkedevents character varying(255)
);


ALTER TABLE public.single_event OWNER TO postgres;

--
-- Name: slew; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.slew (
    entity_id character varying(255) NOT NULL,
    occurrence_from character varying(255),
    slew_manoeuvre character varying(255),
    occurrence_to character varying(255)
);


ALTER TABLE public.slew OWNER TO postgres;

--
-- Name: slew_manoeuvre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.slew_manoeuvre (
    entity_id character varying(255) NOT NULL,
    duration integer NOT NULL,
    final_q0 double precision,
    final_q1 double precision,
    final_q2 double precision,
    final_q3 double precision,
    initial_q0 double precision,
    initial_q1 double precision,
    initial_q2 double precision,
    initial_q3 double precision
);


ALTER TABLE public.slew_manoeuvre OWNER TO postgres;

--
-- Name: source_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.source_file (
    entity_id character varying(255) NOT NULL,
    checksum character varying(255),
    file_author character varying(255),
    filename character varying(255) NOT NULL,
    modification_date timestamp without time zone NOT NULL,
    url character varying(255) NOT NULL
);


ALTER TABLE public.source_file OWNER TO postgres;

--
-- Name: star_map; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.star_map (
    filename character varying(255) NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.star_map OWNER TO postgres;

--
-- Name: start_data_downlink; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.start_data_downlink (
    tx_identifier bigint NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.start_data_downlink OWNER TO postgres;

--
-- Name: start_science_data_suspend; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.start_science_data_suspend (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.start_science_data_suspend OWNER TO postgres;

--
-- Name: state_vector; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.state_vector (
    entity_id character varying(255) NOT NULL,
    acceleration_x double precision,
    acceleration_y double precision,
    acceleration_z double precision,
    instant timestamp without time zone NOT NULL,
    position_x double precision,
    position_y double precision,
    position_z double precision,
    velocity_x double precision,
    velocity_y double precision,
    velocity_z double precision,
    orbit_file character varying(255) NOT NULL
);


ALTER TABLE public.state_vector OWNER TO postgres;

--
-- Name: stop_data_downlink; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stop_data_downlink (
    tx_identifier bigint NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.stop_data_downlink OWNER TO postgres;

--
-- Name: stop_science_data_suspend; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stop_science_data_suspend (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.stop_science_data_suspend OWNER TO postgres;

--
-- Name: sun_angle_constraint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sun_angle_constraint (
    entity_id character varying(255) NOT NULL,
    sun_angle double precision NOT NULL,
    day_of_year integer NOT NULL,
    algorithms_data character varying(255) NOT NULL
);


ALTER TABLE public.sun_angle_constraint OWNER TO postgres;

--
-- Name: target_acquisition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.target_acquisition (
    duration_man bigint NOT NULL,
    q0_final double precision,
    q1_final double precision,
    q2_final double precision,
    q3_final double precision,
    q0_ini double precision,
    q1_ini double precision,
    q2_ini double precision,
    q3_ini double precision,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.target_acquisition OWNER TO postgres;

--
-- Name: tc_activity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tc_activity (
    entity_id character varying(255) NOT NULL,
    duration double precision,
    external_activity_type character varying(255) NOT NULL,
    size bigint,
    version character varying(255),
    source_file character varying(255) NOT NULL
);


ALTER TABLE public.tc_activity OWNER TO postgres;

--
-- Name: tc_defnitions_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tc_defnitions_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.tc_defnitions_file OWNER TO postgres;

--
-- Name: tc_parameter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tc_parameter (
    entity_id character varying(255) NOT NULL,
    data_type character varying(255) NOT NULL,
    default_value character varying(255),
    external_id character varying(255) NOT NULL,
    tc_id character varying(255) NOT NULL,
    tcactivity character varying(255)
);


ALTER TABLE public.tc_parameter OWNER TO postgres;

--
-- Name: transfer_fbf_to_ground; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transfer_fbf_to_ground (
    fbf_init bigint NOT NULL,
    fbf_size bigint NOT NULL,
    nmb_fbf bigint NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.transfer_fbf_to_ground OWNER TO postgres;

--
-- Name: unavailability; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unavailability (
    entity_id character varying(255) NOT NULL,
    unvailability_request character varying(255)
);


ALTER TABLE public.unavailability OWNER TO postgres;

--
-- Name: unavailability_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unavailability_request (
    flexibility_window_start timestamp without time zone,
    flexibility_window_stop timestamp without time zone,
    slot_start timestamp without time zone NOT NULL,
    slot_stop timestamp without time zone NOT NULL,
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.unavailability_request OWNER TO postgres;

--
-- Name: visit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.visit (
    is_uplinked boolean DEFAULT false NOT NULL,
    planned_effectiveness double precision NOT NULL,
    quality_status integer NOT NULL,
    real_effectiveness double precision,
    straylight_flux_threshold double precision NOT NULL,
    entity_id character varying(255) NOT NULL,
    request character varying(255)
);


ALTER TABLE public.visit OWNER TO postgres;

--
-- Name: visit_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.visit_status (
    entity_id character varying(255) NOT NULL,
    time_stamp timestamp without time zone NOT NULL,
    valid_measurement boolean NOT NULL,
    visit_status_file character varying(255) NOT NULL,
    visit character varying(255) NOT NULL
);


ALTER TABLE public.visit_status OWNER TO postgres;

--
-- Name: visit_status_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.visit_status_file (
    entity_id character varying(255) NOT NULL
);


ALTER TABLE public.visit_status_file OWNER TO postgres;

--
-- Name: activity activity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT activity_pkey PRIMARY KEY (entity_id);


--
-- Name: algorithms_data algorithms_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.algorithms_data
    ADD CONSTRAINT algorithms_data_pkey PRIMARY KEY (entity_id);


--
-- Name: algorithms_file algorithms_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.algorithms_file
    ADD CONSTRAINT algorithms_file_pkey PRIMARY KEY (entity_id);


--
-- Name: audit_event audit_event_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.audit_event
    ADD CONSTRAINT audit_event_pkey PRIMARY KEY (audit_event_id);


--
-- Name: binary_resource binary_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.binary_resource
    ADD CONSTRAINT binary_resource_pkey PRIMARY KEY (entity_id);


--
-- Name: boolean_measure boolean_measure_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boolean_measure
    ADD CONSTRAINT boolean_measure_pkey PRIMARY KEY (entity_id);


--
-- Name: configuration_dynamic configuration_dynamic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_dynamic
    ADD CONSTRAINT configuration_dynamic_pkey PRIMARY KEY (entity_id);


--
-- Name: double_measure double_measure_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.double_measure
    ADD CONSTRAINT double_measure_pkey PRIMARY KEY (entity_id);


--
-- Name: downlink downlink_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.downlink
    ADD CONSTRAINT downlink_pkey PRIMARY KEY (entity_id);


--
-- Name: dynamic_parameter dynamic_parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dynamic_parameter
    ADD CONSTRAINT dynamic_parameter_pkey PRIMARY KEY (entity_id);


--
-- Name: external_id_generator external_id_generator_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.external_id_generator
    ADD CONSTRAINT external_id_generator_pkey PRIMARY KEY (entity_id);


--
-- Name: fbftransfer fbftransfer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fbftransfer
    ADD CONSTRAINT fbftransfer_pkey PRIMARY KEY (entity_id);


--
-- Name: flash_based_file flash_based_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flash_based_file
    ADD CONSTRAINT flash_based_file_pkey PRIMARY KEY (entity_id);


--
-- Name: generation_baseline generation_baseline_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generation_baseline
    ADD CONSTRAINT generation_baseline_pkey PRIMARY KEY (entity_id);


--
-- Name: geometric_resource geometric_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.geometric_resource
    ADD CONSTRAINT geometric_resource_pkey PRIMARY KEY (entity_id);


--
-- Name: gsavailability_file gsavailability_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gsavailability_file
    ADD CONSTRAINT gsavailability_file_pkey PRIMARY KEY (entity_id);


--
-- Name: gsavailability gsavailability_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gsavailability
    ADD CONSTRAINT gsavailability_pkey PRIMARY KEY (entity_id);


--
-- Name: hktmparameters_file hktmparameters_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hktmparameters_file
    ADD CONSTRAINT hktmparameters_file_pkey PRIMARY KEY (entity_id);


--
-- Name: hktmparameters hktmparameters_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hktmparameters
    ADD CONSTRAINT hktmparameters_pkey PRIMARY KEY (entity_id);


--
-- Name: idle_target idle_target_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idle_target
    ADD CONSTRAINT idle_target_pkey PRIMARY KEY (entity_id);


--
-- Name: interval_measure interval_measure_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interval_measure
    ADD CONSTRAINT interval_measure_pkey PRIMARY KEY (entity_id);


--
-- Name: interval_resource interval_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interval_resource
    ADD CONSTRAINT interval_resource_pkey PRIMARY KEY (entity_id);


--
-- Name: leapsecond_value leapsecond_value_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leapsecond_value
    ADD CONSTRAINT leapsecond_value_pkey PRIMARY KEY (entity_id);


--
-- Name: leapseconds_file leapseconds_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leapseconds_file
    ADD CONSTRAINT leapseconds_file_pkey PRIMARY KEY (entity_id);


--
-- Name: linked_events linked_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.linked_events
    ADD CONSTRAINT linked_events_pkey PRIMARY KEY (entity_id);


--
-- Name: master_baseline master_baseline_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_baseline
    ADD CONSTRAINT master_baseline_pkey PRIMARY KEY (entity_id);


--
-- Name: master_profile master_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_profile
    ADD CONSTRAINT master_profile_pkey PRIMARY KEY (entity_id);


--
-- Name: measure measure_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measure
    ADD CONSTRAINT measure_pkey PRIMARY KEY (entity_id);


--
-- Name: nominal_observation nominal_observation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nominal_observation
    ADD CONSTRAINT nominal_observation_pkey PRIMARY KEY (entity_id);


--
-- Name: observation_request observation_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.observation_request
    ADD CONSTRAINT observation_request_pkey PRIMARY KEY (entity_id);


--
-- Name: occurrence occurrence_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence
    ADD CONSTRAINT occurrence_pkey PRIMARY KEY (entity_id);


--
-- Name: occurrence_plan_activities occurrence_plan_activities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan_activities
    ADD CONSTRAINT occurrence_plan_activities_pkey PRIMARY KEY (occurrence_plan_id, activities_index);


--
-- Name: occurrence_plan occurrence_plan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan
    ADD CONSTRAINT occurrence_plan_pkey PRIMARY KEY (entity_id);


--
-- Name: occurrence_profile occurrence_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_profile
    ADD CONSTRAINT occurrence_profile_pkey PRIMARY KEY (entity_id);


--
-- Name: orbit_file orbit_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orbit_file
    ADD CONSTRAINT orbit_file_pkey PRIMARY KEY (entity_id);


--
-- Name: parameters_activity parameters_activity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_activity
    ADD CONSTRAINT parameters_activity_pkey PRIMARY KEY (entity_id);


--
-- Name: parameters_ce_visitconfdynamic parameters_ce_visitconfdynamic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_ce_visitconfdynamic
    ADD CONSTRAINT parameters_ce_visitconfdynamic_pkey PRIMARY KEY (entity_id);


--
-- Name: parameters_ce_visitconfdynamic_value_per_type parameters_ce_visitconfdynamic_value_per_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_ce_visitconfdynamic_value_per_type
    ADD CONSTRAINT parameters_ce_visitconfdynamic_value_per_type_pkey PRIMARY KEY (visitconfigurationdynamicceparameter_entity_id, ccd_pos_order, margin_mode, observation_category, visit_order, window_type);


--
-- Name: parameters_defaults_file parameters_defaults_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_defaults_file
    ADD CONSTRAINT parameters_defaults_file_pkey PRIMARY KEY (entity_id);


--
-- Name: parameters_fbf_size_per_type parameters_fbf_size_per_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_fbf_size_per_type
    ADD CONSTRAINT parameters_fbf_size_per_type_pkey PRIMARY KEY (activityparameters_entity_id, observation_category);


--
-- Name: parameters_general_earth_limb_altitude_per_category parameters_general_earth_limb_altitude_per_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_general_earth_limb_altitude_per_category
    ADD CONSTRAINT parameters_general_earth_limb_altitude_per_category_pkey PRIMARY KEY (generalparameters_entity_id, category);


--
-- Name: parameters_general_max_straylight_per_mag parameters_general_max_straylight_per_mag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_general_max_straylight_per_mag
    ADD CONSTRAINT parameters_general_max_straylight_per_mag_pkey PRIMARY KEY (generalparameters_entity_id, mag);


--
-- Name: parameters_general_max_straylight_per_mc_category parameters_general_max_straylight_per_mc_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_general_max_straylight_per_mc_category
    ADD CONSTRAINT parameters_general_max_straylight_per_mc_category_pkey PRIMARY KEY (generalparameters_entity_id, category);


--
-- Name: parameters_general parameters_general_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_general
    ADD CONSTRAINT parameters_general_pkey PRIMARY KEY (entity_id);


--
-- Name: parameters_mps_defaults parameters_mps_defaults_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_mps_defaults
    ADD CONSTRAINT parameters_mps_defaults_pkey PRIMARY KEY (entity_id);


--
-- Name: parameters_obs_category_parameters_per_type parameters_obs_category_parameters_per_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_obs_category_parameters_per_type
    ADD CONSTRAINT parameters_obs_category_parameters_per_type_pkey PRIMARY KEY (mpsdefaultsdata_entity_id, observation_category);


--
-- Name: parameters_visitacq parameters_visitacq_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_visitacq
    ADD CONSTRAINT parameters_visitacq_pkey PRIMARY KEY (entity_id);


--
-- Name: parameters_visitacq_value_per_type parameters_visitacq_value_per_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_visitacq_value_per_type
    ADD CONSTRAINT parameters_visitacq_value_per_type_pkey PRIMARY KEY (visitacquisitionparameter_entity_id, acquisition_type, observation_category, window_type);


--
-- Name: parameters_visitconfdynamic parameters_visitconfdynamic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_visitconfdynamic
    ADD CONSTRAINT parameters_visitconfdynamic_pkey PRIMARY KEY (entity_id);


--
-- Name: parameters_visitconfdynamic_value_per_type parameters_visitconfdynamic_value_per_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_visitconfdynamic_value_per_type
    ADD CONSTRAINT parameters_visitconfdynamic_value_per_type_pkey PRIMARY KEY (visitconfigurationdynamicparameter_entity_id, ccd_pos_order, observation_category, visit_order, window_type);


--
-- Name: payload_request payload_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payload_request
    ADD CONSTRAINT payload_request_pkey PRIMARY KEY (entity_id);


--
-- Name: phase_range phase_range_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phase_range
    ADD CONSTRAINT phase_range_pkey PRIMARY KEY (entity_id);


--
-- Name: planning_file planning_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planning_file
    ADD CONSTRAINT planning_file_pkey PRIMARY KEY (entity_id);


--
-- Name: planning_resource planning_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planning_resource
    ADD CONSTRAINT planning_resource_pkey PRIMARY KEY (entity_id);


--
-- Name: platform_request platform_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.platform_request
    ADD CONSTRAINT platform_request_pkey PRIMARY KEY (entity_id);


--
-- Name: platform_unavailability platform_unavailability_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.platform_unavailability
    ADD CONSTRAINT platform_unavailability_pkey PRIMARY KEY (entity_id);


--
-- Name: program program_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program
    ADD CONSTRAINT program_pkey PRIMARY KEY (entity_id);


--
-- Name: pst_file pst_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pst_file
    ADD CONSTRAINT pst_file_pkey PRIMARY KEY (entity_id);


--
-- Name: pst_value pst_value_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pst_value
    ADD CONSTRAINT pst_value_pkey PRIMARY KEY (entity_id);


--
-- Name: request_exposure_time request_exposure_time_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_exposure_time
    ADD CONSTRAINT request_exposure_time_pkey PRIMARY KEY (entity_id, listofexposuretimes_order);


--
-- Name: request_readout_mode request_readout_mode_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_readout_mode
    ADD CONSTRAINT request_readout_mode_pkey PRIMARY KEY (entity_id, readoutmodes_order);


--
-- Name: request_repetition_period request_repetition_period_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_repetition_period
    ADD CONSTRAINT request_repetition_period_pkey PRIMARY KEY (entity_id, listofrepetitionperiods_order);


--
-- Name: request_target request_target_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_target
    ADD CONSTRAINT request_target_pkey PRIMARY KEY (entity_id, requesttargets_order);


--
-- Name: request_visit_offset request_visit_offset_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_visit_offset
    ADD CONSTRAINT request_visit_offset_pkey PRIMARY KEY (entity_id, visitoffsets_order);


--
-- Name: resource_profile resource_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_profile
    ADD CONSTRAINT resource_profile_pkey PRIMARY KEY (entity_id);


--
-- Name: saa_file saa_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.saa_file
    ADD CONSTRAINT saa_file_pkey PRIMARY KEY (entity_id);


--
-- Name: saa_safe saa_safe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.saa_safe
    ADD CONSTRAINT saa_safe_pkey PRIMARY KEY (entity_id);


--
-- Name: saa_value saa_value_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.saa_value
    ADD CONSTRAINT saa_value_pkey PRIMARY KEY (entity_id);


--
-- Name: safe_acquisition safe_acquisition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.safe_acquisition
    ADD CONSTRAINT safe_acquisition_pkey PRIMARY KEY (entity_id);


--
-- Name: single_event single_event_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.single_event
    ADD CONSTRAINT single_event_pkey PRIMARY KEY (entity_id);


--
-- Name: slew_manoeuvre slew_manoeuvre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.slew_manoeuvre
    ADD CONSTRAINT slew_manoeuvre_pkey PRIMARY KEY (entity_id);


--
-- Name: slew slew_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.slew
    ADD CONSTRAINT slew_pkey PRIMARY KEY (entity_id);


--
-- Name: source_file source_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.source_file
    ADD CONSTRAINT source_file_pkey PRIMARY KEY (entity_id);


--
-- Name: star_map star_map_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.star_map
    ADD CONSTRAINT star_map_pkey PRIMARY KEY (entity_id);


--
-- Name: start_data_downlink start_data_downlink_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.start_data_downlink
    ADD CONSTRAINT start_data_downlink_pkey PRIMARY KEY (entity_id);


--
-- Name: start_science_data_suspend start_science_data_suspend_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.start_science_data_suspend
    ADD CONSTRAINT start_science_data_suspend_pkey PRIMARY KEY (entity_id);


--
-- Name: state_vector state_vector_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.state_vector
    ADD CONSTRAINT state_vector_pkey PRIMARY KEY (entity_id);


--
-- Name: stop_data_downlink stop_data_downlink_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop_data_downlink
    ADD CONSTRAINT stop_data_downlink_pkey PRIMARY KEY (entity_id);


--
-- Name: stop_science_data_suspend stop_science_data_suspend_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop_science_data_suspend
    ADD CONSTRAINT stop_science_data_suspend_pkey PRIMARY KEY (entity_id);


--
-- Name: sun_angle_constraint sun_angle_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sun_angle_constraint
    ADD CONSTRAINT sun_angle_constraint_pkey PRIMARY KEY (entity_id);


--
-- Name: target_acquisition target_acquisition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.target_acquisition
    ADD CONSTRAINT target_acquisition_pkey PRIMARY KEY (entity_id);


--
-- Name: tc_activity tc_activity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tc_activity
    ADD CONSTRAINT tc_activity_pkey PRIMARY KEY (entity_id);


--
-- Name: tc_defnitions_file tc_defnitions_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tc_defnitions_file
    ADD CONSTRAINT tc_defnitions_file_pkey PRIMARY KEY (entity_id);


--
-- Name: tc_parameter tc_parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tc_parameter
    ADD CONSTRAINT tc_parameter_pkey PRIMARY KEY (entity_id);


--
-- Name: transfer_fbf_to_ground transfer_fbf_to_ground_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transfer_fbf_to_ground
    ADD CONSTRAINT transfer_fbf_to_ground_pkey PRIMARY KEY (entity_id);


--
-- Name: occurrence_audit_event uk_1pj077rjp9ggmjt661uydng25; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_audit_event
    ADD CONSTRAINT uk_1pj077rjp9ggmjt661uydng25 UNIQUE (events_audit_event_id);


--
-- Name: payload_request_audit_event uk_2ipcib4ulbpyay7qkuuy4csku; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payload_request_audit_event
    ADD CONSTRAINT uk_2ipcib4ulbpyay7qkuuy4csku UNIQUE (events_audit_event_id);


--
-- Name: program_audit_event uk_2xqptqi0pyi84xvqpp1djy5au; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_audit_event
    ADD CONSTRAINT uk_2xqptqi0pyi84xvqpp1djy5au UNIQUE (events_audit_event_id);


--
-- Name: parameters_mps_defaults uk_3y4fo6ply5s1yxhmv5brcd2kb; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_mps_defaults
    ADD CONSTRAINT uk_3y4fo6ply5s1yxhmv5brcd2kb UNIQUE (source_file);


--
-- Name: occurrence_plan_audit_event uk_7syq2plr3dxyu7d1sflf0fq9e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan_audit_event
    ADD CONSTRAINT uk_7syq2plr3dxyu7d1sflf0fq9e UNIQUE (events_audit_event_id);


--
-- Name: parameters_mps_defaults uk_8wfc3xtm69mcjdnv3kgh6swtb; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_mps_defaults
    ADD CONSTRAINT uk_8wfc3xtm69mcjdnv3kgh6swtb UNIQUE (general_parameters);


--
-- Name: planning_resource_audit_event uk_9wfgeov4977ej87hkbav11pnh; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planning_resource_audit_event
    ADD CONSTRAINT uk_9wfgeov4977ej87hkbav11pnh UNIQUE (events_audit_event_id);


--
-- Name: external_id_generator uk_bhwk6rtmixhuxb7otlclx1pd0; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.external_id_generator
    ADD CONSTRAINT uk_bhwk6rtmixhuxb7otlclx1pd0 UNIQUE (external_id_counter);


--
-- Name: occurrence_plan_activities uk_goahvm5o4ptdueaejyta24pxw; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan_activities
    ADD CONSTRAINT uk_goahvm5o4ptdueaejyta24pxw UNIQUE (activity_id);


--
-- Name: master_baseline uk_l5cg798q95se4gehy8w97lc89; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_baseline
    ADD CONSTRAINT uk_l5cg798q95se4gehy8w97lc89 UNIQUE (generation_baseline_id);


--
-- Name: occurrence_plan uk_micpmktj4lxngxqufw66c8wme; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan
    ADD CONSTRAINT uk_micpmktj4lxngxqufw66c8wme UNIQUE (filename);


--
-- Name: leapsecond_value uk_n7f76e986rqn5dbhc8y0nem45; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leapsecond_value
    ADD CONSTRAINT uk_n7f76e986rqn5dbhc8y0nem45 UNIQUE (date);


--
-- Name: algorithms_data uk_n9owxl3ps3kybuxj0y3c9na1f; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.algorithms_data
    ADD CONSTRAINT uk_n9owxl3ps3kybuxj0y3c9na1f UNIQUE (algorithms_file);


--
-- Name: activity_audit_event uk_orfr4v58gj3xqkmi6ee35q45u; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_audit_event
    ADD CONSTRAINT uk_orfr4v58gj3xqkmi6ee35q45u UNIQUE (events_audit_event_id);


--
-- Name: parameters_mps_defaults uk_pm938eukn9xerdx1n4a0pa2uv; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_mps_defaults
    ADD CONSTRAINT uk_pm938eukn9xerdx1n4a0pa2uv UNIQUE (activity_parameters);


--
-- Name: flash_based_file uk_pp1f5renjoslel8spn52ahwgf; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flash_based_file
    ADD CONSTRAINT uk_pp1f5renjoslel8spn52ahwgf UNIQUE (logical_address);


--
-- Name: platform_request_audit_event uk_rkbl74gu5ggeclax06feabpsp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.platform_request_audit_event
    ADD CONSTRAINT uk_rkbl74gu5ggeclax06feabpsp UNIQUE (events_audit_event_id);


--
-- Name: unavailability unavailability_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unavailability
    ADD CONSTRAINT unavailability_pkey PRIMARY KEY (entity_id);


--
-- Name: unavailability_request unavailability_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unavailability_request
    ADD CONSTRAINT unavailability_request_pkey PRIMARY KEY (entity_id);


--
-- Name: visit visit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visit
    ADD CONSTRAINT visit_pkey PRIMARY KEY (entity_id);


--
-- Name: visit_status_file visit_status_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visit_status_file
    ADD CONSTRAINT visit_status_file_pkey PRIMARY KEY (entity_id);


--
-- Name: visit_status visit_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visit_status
    ADD CONSTRAINT visit_status_pkey PRIMARY KEY (entity_id);


--
-- Name: activity_occurrence_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX activity_occurrence_fkey ON public.activity USING btree (occurrence);


--
-- Name: measure_resourceprofile_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX measure_resourceprofile_fkey ON public.measure USING btree (resourceprofile);


--
-- Name: occurrence_plan_activities_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX occurrence_plan_activities_fkey ON public.occurrence_plan_activities USING btree (activity_id);


--
-- Name: occurrence_profile_occurrence_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX occurrence_profile_occurrence_fkey ON public.occurrence_profile USING btree (occurrence);


--
-- Name: single_event_linkedevents_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX single_event_linkedevents_fkey ON public.single_event USING btree (linkedevents);


--
-- Name: visit_status_visit_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX visit_status_visit_fkey ON public.visit_status USING btree (visit);


--
-- Name: interval_resource fk_13x7wxdlbpam9gms4o9uo2hs4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interval_resource
    ADD CONSTRAINT fk_13x7wxdlbpam9gms4o9uo2hs4 FOREIGN KEY (entity_id) REFERENCES public.planning_resource(entity_id);


--
-- Name: downlink fk_19nw5uywubiernr1gbavw787u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.downlink
    ADD CONSTRAINT fk_19nw5uywubiernr1gbavw787u FOREIGN KEY (entity_id) REFERENCES public.single_event(entity_id);


--
-- Name: occurrence_plan_audit_event fk_1ewq1uit3n31xod2fqybxkc8i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan_audit_event
    ADD CONSTRAINT fk_1ewq1uit3n31xod2fqybxkc8i FOREIGN KEY (occurrence_plan_entity_id) REFERENCES public.occurrence_plan(entity_id);


--
-- Name: star_map fk_1nad4xdd9nqxr8l14m25ms1fa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.star_map
    ADD CONSTRAINT fk_1nad4xdd9nqxr8l14m25ms1fa FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: occurrence_audit_event fk_1pj077rjp9ggmjt661uydng25; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_audit_event
    ADD CONSTRAINT fk_1pj077rjp9ggmjt661uydng25 FOREIGN KEY (events_audit_event_id) REFERENCES public.audit_event(audit_event_id);


--
-- Name: parameters_visitacq fk_1uf1qch3xp37sgomcl6i50n5e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_visitacq
    ADD CONSTRAINT fk_1uf1qch3xp37sgomcl6i50n5e FOREIGN KEY (visit_acquisition_parameters) REFERENCES public.parameters_activity(entity_id);


--
-- Name: activity fk_1xfl43axuingljl6ng7vvp2rn; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT fk_1xfl43axuingljl6ng7vvp2rn FOREIGN KEY (occurrence) REFERENCES public.occurrence(entity_id);


--
-- Name: hktmparameters_file fk_2gi5yj9cljjcs3k4jlgn1w8pt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hktmparameters_file
    ADD CONSTRAINT fk_2gi5yj9cljjcs3k4jlgn1w8pt FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: parameters_fbf_size_per_type fk_2hs76k2fvcd9nic4evotbvp5x; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_fbf_size_per_type
    ADD CONSTRAINT fk_2hs76k2fvcd9nic4evotbvp5x FOREIGN KEY (activityparameters_entity_id) REFERENCES public.parameters_activity(entity_id);


--
-- Name: parameters_corrupted_fbfs fk_2im4oxpatrlpbvtr09t337y8x; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_corrupted_fbfs
    ADD CONSTRAINT fk_2im4oxpatrlpbvtr09t337y8x FOREIGN KEY (mpsdefaultsdata_entity_id) REFERENCES public.parameters_mps_defaults(entity_id);


--
-- Name: payload_request_audit_event fk_2ipcib4ulbpyay7qkuuy4csku; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payload_request_audit_event
    ADD CONSTRAINT fk_2ipcib4ulbpyay7qkuuy4csku FOREIGN KEY (events_audit_event_id) REFERENCES public.audit_event(audit_event_id);


--
-- Name: start_science_data_suspend fk_2lcukn4wssudjpar25tgj5yfu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.start_science_data_suspend
    ADD CONSTRAINT fk_2lcukn4wssudjpar25tgj5yfu FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: saa_safe fk_2vsasfmchrfy7abt9e2gjmrxs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.saa_safe
    ADD CONSTRAINT fk_2vsasfmchrfy7abt9e2gjmrxs FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: request_number_of_images fk_2vvx20csnb5agtl6nledh37s4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_number_of_images
    ADD CONSTRAINT fk_2vvx20csnb5agtl6nledh37s4 FOREIGN KEY (entity_id) REFERENCES public.observation_request(entity_id);


--
-- Name: program_audit_event fk_2xqptqi0pyi84xvqpp1djy5au; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_audit_event
    ADD CONSTRAINT fk_2xqptqi0pyi84xvqpp1djy5au FOREIGN KEY (events_audit_event_id) REFERENCES public.audit_event(audit_event_id);


--
-- Name: interval_measure fk_38woa0n8210ul18cpyep4jxny; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interval_measure
    ADD CONSTRAINT fk_38woa0n8210ul18cpyep4jxny FOREIGN KEY (entity_id) REFERENCES public.measure(entity_id);


--
-- Name: parameters_mps_defaults fk_3y4fo6ply5s1yxhmv5brcd2kb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_mps_defaults
    ADD CONSTRAINT fk_3y4fo6ply5s1yxhmv5brcd2kb FOREIGN KEY (source_file) REFERENCES public.parameters_defaults_file(entity_id);


--
-- Name: target_acquisition fk_44c5mo3ks86b6r6t0ajhfpxy7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.target_acquisition
    ADD CONSTRAINT fk_44c5mo3ks86b6r6t0ajhfpxy7 FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: fbftransfer fk_4h8pcvh1f9hm4scp1l6ulycvh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fbftransfer
    ADD CONSTRAINT fk_4h8pcvh1f9hm4scp1l6ulycvh FOREIGN KEY (entity_id) REFERENCES public.single_event(entity_id);


--
-- Name: occurrence_plan fk_4jj6dwp598jfhwgmes2dflbdn; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan
    ADD CONSTRAINT fk_4jj6dwp598jfhwgmes2dflbdn FOREIGN KEY (generation_baseline) REFERENCES public.generation_baseline(entity_id);


--
-- Name: measure fk_5mn0ki53f02svsmr1hvuu9nsu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measure
    ADD CONSTRAINT fk_5mn0ki53f02svsmr1hvuu9nsu FOREIGN KEY (resourceprofile) REFERENCES public.resource_profile(entity_id);


--
-- Name: leapseconds_file fk_5xslsq6ax2osmyfbgb98ua40t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leapseconds_file
    ADD CONSTRAINT fk_5xslsq6ax2osmyfbgb98ua40t FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: visit_status_file fk_60kr10cn1kw3m2b00xmycvbv7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visit_status_file
    ADD CONSTRAINT fk_60kr10cn1kw3m2b00xmycvbv7 FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: planning_file fk_6bq79oqruwx8m3gndrwimsops; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planning_file
    ADD CONSTRAINT fk_6bq79oqruwx8m3gndrwimsops FOREIGN KEY (entity_id) REFERENCES public.source_file(entity_id);


--
-- Name: parameters_visitacq_value_per_type fk_6hmhlmhrsp71ncaqwql9lawbc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_visitacq_value_per_type
    ADD CONSTRAINT fk_6hmhlmhrsp71ncaqwql9lawbc FOREIGN KEY (visitacquisitionparameter_entity_id) REFERENCES public.parameters_visitacq(entity_id);


--
-- Name: transfer_fbf_to_ground fk_6yw184t6lne079dwsxsj16mmf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transfer_fbf_to_ground
    ADD CONSTRAINT fk_6yw184t6lne079dwsxsj16mmf FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: observation_request fk_799ywxt6a8tj2yq2p349dm4bv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.observation_request
    ADD CONSTRAINT fk_799ywxt6a8tj2yq2p349dm4bv FOREIGN KEY (sourcefile_entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: phase_range_effectiveness fk_7gfofpphsxgs8h4uaebfy3jr3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phase_range_effectiveness
    ADD CONSTRAINT fk_7gfofpphsxgs8h4uaebfy3jr3 FOREIGN KEY (entity_id) REFERENCES public.visit(entity_id);


--
-- Name: occurrence_plan_audit_event fk_7syq2plr3dxyu7d1sflf0fq9e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan_audit_event
    ADD CONSTRAINT fk_7syq2plr3dxyu7d1sflf0fq9e FOREIGN KEY (events_audit_event_id) REFERENCES public.audit_event(audit_event_id);


--
-- Name: observation_request fk_8v7ra2mscq5lfbxbyqusuxmwq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.observation_request
    ADD CONSTRAINT fk_8v7ra2mscq5lfbxbyqusuxmwq FOREIGN KEY (entity_id) REFERENCES public.payload_request(entity_id);


--
-- Name: sun_angle_constraint fk_8vt0sna5s4flelnp3a5h7vaw1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sun_angle_constraint
    ADD CONSTRAINT fk_8vt0sna5s4flelnp3a5h7vaw1 FOREIGN KEY (algorithms_data) REFERENCES public.algorithms_data(entity_id);


--
-- Name: parameters_mps_defaults fk_8wfc3xtm69mcjdnv3kgh6swtb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_mps_defaults
    ADD CONSTRAINT fk_8wfc3xtm69mcjdnv3kgh6swtb FOREIGN KEY (general_parameters) REFERENCES public.parameters_general(entity_id);


--
-- Name: occurrence_profile fk_9cbou7iysg2pxd7637wque4w2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_profile
    ADD CONSTRAINT fk_9cbou7iysg2pxd7637wque4w2 FOREIGN KEY (entity_id) REFERENCES public.resource_profile(entity_id);


--
-- Name: payload_request_audit_event fk_9juo1rsf3vt0jdbxcbm5aicsv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payload_request_audit_event
    ADD CONSTRAINT fk_9juo1rsf3vt0jdbxcbm5aicsv FOREIGN KEY (payload_request_entity_id) REFERENCES public.payload_request(entity_id);


--
-- Name: visit fk_9kgl9e27k5j1nxi4yfkixg8hs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visit
    ADD CONSTRAINT fk_9kgl9e27k5j1nxi4yfkixg8hs FOREIGN KEY (request) REFERENCES public.observation_request(entity_id);


--
-- Name: algorithms_file fk_9v9etdad85skn3aos2rbv8ckl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.algorithms_file
    ADD CONSTRAINT fk_9v9etdad85skn3aos2rbv8ckl FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: planning_resource_audit_event fk_9wfgeov4977ej87hkbav11pnh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planning_resource_audit_event
    ADD CONSTRAINT fk_9wfgeov4977ej87hkbav11pnh FOREIGN KEY (events_audit_event_id) REFERENCES public.audit_event(audit_event_id);


--
-- Name: double_measure fk_9y4incha4vomv37kc5h3uqe47; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.double_measure
    ADD CONSTRAINT fk_9y4incha4vomv37kc5h3uqe47 FOREIGN KEY (entity_id) REFERENCES public.measure(entity_id);


--
-- Name: occurrence_audit_event fk_9ype79ij13qf70lvi8ec5e2rj; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_audit_event
    ADD CONSTRAINT fk_9ype79ij13qf70lvi8ec5e2rj FOREIGN KEY (occurrence_entity_id) REFERENCES public.occurrence(entity_id);


--
-- Name: gsavailability fk_a15gcas7j28itseep1212rgsf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gsavailability
    ADD CONSTRAINT fk_a15gcas7j28itseep1212rgsf FOREIGN KEY (gsavailability_file) REFERENCES public.gsavailability_file(entity_id);


--
-- Name: hktmparameters fk_a2xm9bf5vkgp9kyo91kyvev3q; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hktmparameters
    ADD CONSTRAINT fk_a2xm9bf5vkgp9kyo91kyvev3q FOREIGN KEY (hktmparameters_file) REFERENCES public.hktmparameters_file(entity_id);


--
-- Name: request_readout_mode fk_am86p25j1xo1q2r0bctjkptvu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_readout_mode
    ADD CONSTRAINT fk_am86p25j1xo1q2r0bctjkptvu FOREIGN KEY (entity_id) REFERENCES public.observation_request(entity_id);


--
-- Name: downlink fk_ap83pum9wc6at0om8doekm0wi; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.downlink
    ADD CONSTRAINT fk_ap83pum9wc6at0om8doekm0wi FOREIGN KEY (gs_availability) REFERENCES public.gsavailability(entity_id);


--
-- Name: unavailability fk_b10knngnccgm47wsigkupxxw2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unavailability
    ADD CONSTRAINT fk_b10knngnccgm47wsigkupxxw2 FOREIGN KEY (unvailability_request) REFERENCES public.unavailability_request(entity_id);


--
-- Name: dynamic_parameter fk_b7n86qi6qvuc4m9i988x0ig3w; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dynamic_parameter
    ADD CONSTRAINT fk_b7n86qi6qvuc4m9i988x0ig3w FOREIGN KEY (configurationdynamicactivity) REFERENCES public.configuration_dynamic(entity_id);


--
-- Name: slew fk_bwnwo66x0y45wniqbuyroe3ec; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.slew
    ADD CONSTRAINT fk_bwnwo66x0y45wniqbuyroe3ec FOREIGN KEY (entity_id) REFERENCES public.single_event(entity_id);


--
-- Name: planning_resource_audit_event fk_c9pefubeoc9n9knylfj8g2bh6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planning_resource_audit_event
    ADD CONSTRAINT fk_c9pefubeoc9n9knylfj8g2bh6 FOREIGN KEY (planning_resource_entity_id) REFERENCES public.planning_resource(entity_id);


--
-- Name: fbf_transfer_disabled_fbfs fk_ciae9d9nrjraulpxqbocec76s; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fbf_transfer_disabled_fbfs
    ADD CONSTRAINT fk_ciae9d9nrjraulpxqbocec76s FOREIGN KEY (entity_id) REFERENCES public.fbftransfer(entity_id);


--
-- Name: unavailability fk_crrffn49u06f7qa66kw7ot4mc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unavailability
    ADD CONSTRAINT fk_crrffn49u06f7qa66kw7ot4mc FOREIGN KEY (entity_id) REFERENCES public.single_event(entity_id);


--
-- Name: stop_science_data_suspend fk_d4bswrqmhmp890kqqfcl64cq8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop_science_data_suspend
    ADD CONSTRAINT fk_d4bswrqmhmp890kqqfcl64cq8 FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: payload_request fk_dryik45v8r1j326tuhn8vp6sh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payload_request
    ADD CONSTRAINT fk_dryik45v8r1j326tuhn8vp6sh FOREIGN KEY (program) REFERENCES public.program(entity_id);


--
-- Name: orbit_file fk_du3hh9rtfkm0n53fyyrbx2nko; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orbit_file
    ADD CONSTRAINT fk_du3hh9rtfkm0n53fyyrbx2nko FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: parameters_obs_category_parameters_per_type fk_e9jm96y241xloceahahdrvnkw; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_obs_category_parameters_per_type
    ADD CONSTRAINT fk_e9jm96y241xloceahahdrvnkw FOREIGN KEY (mpsdefaultsdata_entity_id) REFERENCES public.parameters_mps_defaults(entity_id);


--
-- Name: parameters_visitconfdynamic fk_ebr0ebxi1i17ctp1horcnru9o; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_visitconfdynamic
    ADD CONSTRAINT fk_ebr0ebxi1i17ctp1horcnru9o FOREIGN KEY (dynamic_parameters) REFERENCES public.parameters_activity(entity_id);


--
-- Name: slew fk_f7jac347be15i9l5eroiyrg5w; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.slew
    ADD CONSTRAINT fk_f7jac347be15i9l5eroiyrg5w FOREIGN KEY (occurrence_from) REFERENCES public.occurrence(entity_id);


--
-- Name: gen_baseline_src_file fk_g0y3esvfgo1awjsncda328a2o; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gen_baseline_src_file
    ADD CONSTRAINT fk_g0y3esvfgo1awjsncda328a2o FOREIGN KEY (source_file) REFERENCES public.source_file(entity_id);


--
-- Name: state_vector fk_gbjmtg4834t3ty8hppq2ytmys; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.state_vector
    ADD CONSTRAINT fk_gbjmtg4834t3ty8hppq2ytmys FOREIGN KEY (orbit_file) REFERENCES public.orbit_file(entity_id);


--
-- Name: occurrence_plan_activities fk_goahvm5o4ptdueaejyta24pxw; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan_activities
    ADD CONSTRAINT fk_goahvm5o4ptdueaejyta24pxw FOREIGN KEY (activity_id) REFERENCES public.activity(entity_id);


--
-- Name: parameters_general_max_straylight_per_mc_category fk_gs236fndtfe9oxmpcp8fueobu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_general_max_straylight_per_mc_category
    ADD CONSTRAINT fk_gs236fndtfe9oxmpcp8fueobu FOREIGN KEY (generalparameters_entity_id) REFERENCES public.parameters_general(entity_id);


--
-- Name: audit_event fk_h4ua5bt1rp0fkppwomb1w39wx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.audit_event
    ADD CONSTRAINT fk_h4ua5bt1rp0fkppwomb1w39wx FOREIGN KEY (source_file_id) REFERENCES public.source_file(entity_id);


--
-- Name: flash_based_file fk_hjlrujnltf2vpbuhiuvj4owng; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flash_based_file
    ADD CONSTRAINT fk_hjlrujnltf2vpbuhiuvj4owng FOREIGN KEY (activity) REFERENCES public.nominal_observation(entity_id);


--
-- Name: parameters_general_earth_limb_altitude_per_category fk_hql7xl2lpj385rj9jj01qq8nl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_general_earth_limb_altitude_per_category
    ADD CONSTRAINT fk_hql7xl2lpj385rj9jj01qq8nl FOREIGN KEY (generalparameters_entity_id) REFERENCES public.parameters_general(entity_id);


--
-- Name: parameters_ce_visitconfdynamic_value_per_type fk_i5ukpo3v5j898gbmi0oe3ucfp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_ce_visitconfdynamic_value_per_type
    ADD CONSTRAINT fk_i5ukpo3v5j898gbmi0oe3ucfp FOREIGN KEY (visitconfigurationdynamicceparameter_entity_id) REFERENCES public.parameters_ce_visitconfdynamic(entity_id);


--
-- Name: request_repetition_period fk_i74ps1lcbwf2h96fkqcb8qv06; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_repetition_period
    ADD CONSTRAINT fk_i74ps1lcbwf2h96fkqcb8qv06 FOREIGN KEY (entity_id) REFERENCES public.observation_request(entity_id);


--
-- Name: occurrence fk_imc6ajiqufg40tvpghajkmbo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence
    ADD CONSTRAINT fk_imc6ajiqufg40tvpghajkmbo FOREIGN KEY (generation_baseline) REFERENCES public.generation_baseline(entity_id);


--
-- Name: parameters_ce_visitconfdynamic fk_ioqj7eempq2n6pn3i8m09ifge; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_ce_visitconfdynamic
    ADD CONSTRAINT fk_ioqj7eempq2n6pn3i8m09ifge FOREIGN KEY (dynamic_ce_parameters) REFERENCES public.parameters_activity(entity_id);


--
-- Name: observation_id fk_iqn263j1eug3skapffs4p13fq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.observation_id
    ADD CONSTRAINT fk_iqn263j1eug3skapffs4p13fq FOREIGN KEY (entity_id) REFERENCES public.fbftransfer(entity_id);


--
-- Name: slew fk_ixolts71em0ael1ibl7ghfdj7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.slew
    ADD CONSTRAINT fk_ixolts71em0ael1ibl7ghfdj7 FOREIGN KEY (slew_manoeuvre) REFERENCES public.slew_manoeuvre(entity_id);


--
-- Name: linked_events fk_jmp8kwsg4sa27x9vlrjsv54ps; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.linked_events
    ADD CONSTRAINT fk_jmp8kwsg4sa27x9vlrjsv54ps FOREIGN KEY (entity_id) REFERENCES public.occurrence(entity_id);


--
-- Name: visit_status fk_jxw671a4sc1w7dx9lueiaklw9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visit_status
    ADD CONSTRAINT fk_jxw671a4sc1w7dx9lueiaklw9 FOREIGN KEY (visit) REFERENCES public.visit(entity_id);


--
-- Name: boolean_measure fk_k6woe80mqosi79nfvlf9etgxd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boolean_measure
    ADD CONSTRAINT fk_k6woe80mqosi79nfvlf9etgxd FOREIGN KEY (entity_id) REFERENCES public.measure(entity_id);


--
-- Name: request_target fk_kbayvjpjpe84qk0jspovisl8o; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_target
    ADD CONSTRAINT fk_kbayvjpjpe84qk0jspovisl8o FOREIGN KEY (entity_id) REFERENCES public.observation_request(entity_id);


--
-- Name: pst_value fk_knkkc5km2c5jywsffqnr1il99; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pst_value
    ADD CONSTRAINT fk_knkkc5km2c5jywsffqnr1il99 FOREIGN KEY (pst_file) REFERENCES public.pst_file(entity_id);


--
-- Name: occurrence_profile fk_kxjcyq7139toro78gf8uiphpt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_profile
    ADD CONSTRAINT fk_kxjcyq7139toro78gf8uiphpt FOREIGN KEY (occurrence) REFERENCES public.occurrence(entity_id);


--
-- Name: master_baseline fk_l5cg798q95se4gehy8w97lc89; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_baseline
    ADD CONSTRAINT fk_l5cg798q95se4gehy8w97lc89 FOREIGN KEY (generation_baseline_id) REFERENCES public.generation_baseline(entity_id);


--
-- Name: occurrence_plan_activities fk_l8yj7da7bu91jogynvxiv2fnv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occurrence_plan_activities
    ADD CONSTRAINT fk_l8yj7da7bu91jogynvxiv2fnv FOREIGN KEY (occurrence_plan_id) REFERENCES public.occurrence_plan(entity_id);


--
-- Name: request_visit_offset fk_lf6nmpv6avaf8tc5j8xq6i2fi; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_visit_offset
    ADD CONSTRAINT fk_lf6nmpv6avaf8tc5j8xq6i2fi FOREIGN KEY (entity_id) REFERENCES public.observation_request(entity_id);


--
-- Name: activity_audit_event fk_lxvlr2he034fyukchbloqq7ws; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_audit_event
    ADD CONSTRAINT fk_lxvlr2he034fyukchbloqq7ws FOREIGN KEY (activity_entity_id) REFERENCES public.activity(entity_id);


--
-- Name: pst_file fk_m2mpp1jbodr123192eq1h1gqv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pst_file
    ADD CONSTRAINT fk_m2mpp1jbodr123192eq1h1gqv FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: geometric_resource fk_mj78kqc3nghe103boywk6y9ib; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.geometric_resource
    ADD CONSTRAINT fk_mj78kqc3nghe103boywk6y9ib FOREIGN KEY (entity_id) REFERENCES public.planning_resource(entity_id);


--
-- Name: single_event fk_mtnfq4xjqfigmdp0nj0h8xwx8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.single_event
    ADD CONSTRAINT fk_mtnfq4xjqfigmdp0nj0h8xwx8 FOREIGN KEY (linkedevents) REFERENCES public.linked_events(entity_id);


--
-- Name: program_audit_event fk_n2nh1xdcbly9g4ep7m2tx0ca3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_audit_event
    ADD CONSTRAINT fk_n2nh1xdcbly9g4ep7m2tx0ca3 FOREIGN KEY (program_entity_id) REFERENCES public.program(entity_id);


--
-- Name: platform_request_audit_event fk_n3oep4xsligo9ml20d9jv4otm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.platform_request_audit_event
    ADD CONSTRAINT fk_n3oep4xsligo9ml20d9jv4otm FOREIGN KEY (platform_request_entity_id) REFERENCES public.platform_request(entity_id);


--
-- Name: algorithms_data fk_n9owxl3ps3kybuxj0y3c9na1f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.algorithms_data
    ADD CONSTRAINT fk_n9owxl3ps3kybuxj0y3c9na1f FOREIGN KEY (algorithms_file) REFERENCES public.algorithms_file(entity_id);


--
-- Name: binary_resource fk_nbpkom6x3cwo464n7hme8d46r; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.binary_resource
    ADD CONSTRAINT fk_nbpkom6x3cwo464n7hme8d46r FOREIGN KEY (entity_id) REFERENCES public.planning_resource(entity_id);


--
-- Name: parameters_general_programme_time_allocation_period fk_nko6c8p90x9mt5wjmj8muh285; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_general_programme_time_allocation_period
    ADD CONSTRAINT fk_nko6c8p90x9mt5wjmj8muh285 FOREIGN KEY (generalparameters_entity_id) REFERENCES public.parameters_general(entity_id);


--
-- Name: single_event fk_ntgqwoyiaghl7b1hoc7v330e0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.single_event
    ADD CONSTRAINT fk_ntgqwoyiaghl7b1hoc7v330e0 FOREIGN KEY (entity_id) REFERENCES public.occurrence(entity_id);


--
-- Name: phase_range fk_o2ns5uvw5ky4dlsrbnwaplkfc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phase_range
    ADD CONSTRAINT fk_o2ns5uvw5ky4dlsrbnwaplkfc FOREIGN KEY (request_id) REFERENCES public.observation_request(entity_id);


--
-- Name: safe_acquisition fk_o2r245kdrq4et4445v8br0yon; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.safe_acquisition
    ADD CONSTRAINT fk_o2r245kdrq4et4445v8br0yon FOREIGN KEY (entity_id) REFERENCES public.single_event(entity_id);


--
-- Name: gen_baseline_src_file fk_oflq0w4hogeisxlx03aj3w8s; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gen_baseline_src_file
    ADD CONSTRAINT fk_oflq0w4hogeisxlx03aj3w8s FOREIGN KEY (generation_baseline) REFERENCES public.generation_baseline(entity_id);


--
-- Name: visit fk_oidg00sj683oa8a3uoh9l1g6k; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visit
    ADD CONSTRAINT fk_oidg00sj683oa8a3uoh9l1g6k FOREIGN KEY (entity_id) REFERENCES public.single_event(entity_id);


--
-- Name: activity_audit_event fk_orfr4v58gj3xqkmi6ee35q45u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_audit_event
    ADD CONSTRAINT fk_orfr4v58gj3xqkmi6ee35q45u FOREIGN KEY (events_audit_event_id) REFERENCES public.audit_event(audit_event_id);


--
-- Name: tc_defnitions_file fk_p8rh58e85euvveosbtw5ey9hf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tc_defnitions_file
    ADD CONSTRAINT fk_p8rh58e85euvveosbtw5ey9hf FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: unavailability_request fk_pbc3acy4qdbu44qlgbeswhso4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unavailability_request
    ADD CONSTRAINT fk_pbc3acy4qdbu44qlgbeswhso4 FOREIGN KEY (entity_id) REFERENCES public.platform_request(entity_id);


--
-- Name: parameters_mps_defaults fk_pm938eukn9xerdx1n4a0pa2uv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_mps_defaults
    ADD CONSTRAINT fk_pm938eukn9xerdx1n4a0pa2uv FOREIGN KEY (activity_parameters) REFERENCES public.parameters_activity(entity_id);


--
-- Name: visit_status fk_pmlttlsvdwb5huvxcbdixiak5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.visit_status
    ADD CONSTRAINT fk_pmlttlsvdwb5huvxcbdixiak5 FOREIGN KEY (visit_status_file) REFERENCES public.visit_status_file(entity_id);


--
-- Name: parameters_general_max_straylight_per_mag fk_q6qonprda631rlsqn5lb8utvo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_general_max_straylight_per_mag
    ADD CONSTRAINT fk_q6qonprda631rlsqn5lb8utvo FOREIGN KEY (generalparameters_entity_id) REFERENCES public.parameters_general(entity_id);


--
-- Name: parameters_visitconfdynamic_value_per_type fk_q78v4y79fv0qk7spkb9piqqc7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_visitconfdynamic_value_per_type
    ADD CONSTRAINT fk_q78v4y79fv0qk7spkb9piqqc7 FOREIGN KEY (visitconfigurationdynamicparameter_entity_id) REFERENCES public.parameters_visitconfdynamic(entity_id);


--
-- Name: nominal_observation fk_qdk267d2lbekiribd41s6kfbf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nominal_observation
    ADD CONSTRAINT fk_qdk267d2lbekiribd41s6kfbf FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: leapsecond_value fk_r0gky14pmw4mhafxq3jp7dndf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leapsecond_value
    ADD CONSTRAINT fk_r0gky14pmw4mhafxq3jp7dndf FOREIGN KEY (leapseconds_file) REFERENCES public.leapseconds_file(entity_id);


--
-- Name: slew fk_r5qgwuj4vcfey0k8yliklrckf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.slew
    ADD CONSTRAINT fk_r5qgwuj4vcfey0k8yliklrckf FOREIGN KEY (occurrence_to) REFERENCES public.occurrence(entity_id);


--
-- Name: request_exposure_time fk_r88exqjxl5w8rfko9p71k9ij8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.request_exposure_time
    ADD CONSTRAINT fk_r88exqjxl5w8rfko9p71k9ij8 FOREIGN KEY (entity_id) REFERENCES public.observation_request(entity_id);


--
-- Name: platform_request_audit_event fk_rkbl74gu5ggeclax06feabpsp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.platform_request_audit_event
    ADD CONSTRAINT fk_rkbl74gu5ggeclax06feabpsp FOREIGN KEY (events_audit_event_id) REFERENCES public.audit_event(audit_event_id);


--
-- Name: master_profile fk_rl66b55vbpx36t0yf53xmk07i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_profile
    ADD CONSTRAINT fk_rl66b55vbpx36t0yf53xmk07i FOREIGN KEY (entity_id) REFERENCES public.resource_profile(entity_id);


--
-- Name: start_data_downlink fk_rsqgxwygsig4286k6cif09pft; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.start_data_downlink
    ADD CONSTRAINT fk_rsqgxwygsig4286k6cif09pft FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: tc_parameter fk_s36fyh9j2db165ueb3vbwfgrp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tc_parameter
    ADD CONSTRAINT fk_s36fyh9j2db165ueb3vbwfgrp FOREIGN KEY (tcactivity) REFERENCES public.tc_activity(entity_id);


--
-- Name: gsavailability_file fk_s48kmdowr4cysnlygfgog9t5m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gsavailability_file
    ADD CONSTRAINT fk_s48kmdowr4cysnlygfgog9t5m FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: saa_value fk_scs1d2c2vnxpdoh00myi2fbuu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.saa_value
    ADD CONSTRAINT fk_scs1d2c2vnxpdoh00myi2fbuu FOREIGN KEY (saa_file) REFERENCES public.saa_file(entity_id);


--
-- Name: parameters_defaults_file fk_sm6pffo463b85t4o7sou5qqpg; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parameters_defaults_file
    ADD CONSTRAINT fk_sm6pffo463b85t4o7sou5qqpg FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: stop_data_downlink fk_ssw6s8wd7m7uslmedtik1ycvm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop_data_downlink
    ADD CONSTRAINT fk_ssw6s8wd7m7uslmedtik1ycvm FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: saa_file fk_svkr73cw10d1623w29ln00mae; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.saa_file
    ADD CONSTRAINT fk_svkr73cw10d1623w29ln00mae FOREIGN KEY (entity_id) REFERENCES public.planning_file(entity_id);


--
-- Name: tc_activity fk_t4g0ysko4a06ay7s8huq09rmj; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tc_activity
    ADD CONSTRAINT fk_t4g0ysko4a06ay7s8huq09rmj FOREIGN KEY (source_file) REFERENCES public.tc_defnitions_file(entity_id);


--
-- Name: resource_profile fk_t6r5m08xb9f6dkppi6k1x36pu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_profile
    ADD CONSTRAINT fk_t6r5m08xb9f6dkppi6k1x36pu FOREIGN KEY (resource) REFERENCES public.planning_resource(entity_id);


--
-- Name: platform_unavailability fk_t89qoxitj48d59ocwhq39pbed; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.platform_unavailability
    ADD CONSTRAINT fk_t89qoxitj48d59ocwhq39pbed FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: configuration_dynamic fk_thq7wne08wsiffrfibg9m6oxu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configuration_dynamic
    ADD CONSTRAINT fk_thq7wne08wsiffrfibg9m6oxu FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: idle_target fk_tnkh4veddnt24kkvgtmkgn9ml; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idle_target
    ADD CONSTRAINT fk_tnkh4veddnt24kkvgtmkgn9ml FOREIGN KEY (entity_id) REFERENCES public.activity(entity_id);


--
-- Name: phase_range_effectiveness fk_toxu5h8qcq4cpbm01evfqpx5f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phase_range_effectiveness
    ADD CONSTRAINT fk_toxu5h8qcq4cpbm01evfqpx5f FOREIGN KEY (phase_range) REFERENCES public.phase_range(entity_id);


--
-- PostgreSQL database dump complete
--

