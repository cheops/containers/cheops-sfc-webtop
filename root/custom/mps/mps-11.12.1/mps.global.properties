######## JVM Properties ########
# 
# These are the properties that can be configured to launch the JVM
#

# Command line arguments -- see https://docs.oracle.com/javase/8/docs/technotes/tools/unix/java.html#BABDJJFI
mps.server.vmArgs=-Duser.timezone=UTC -Xmx3g
mps.hmi.vmArgs=-Duser.timezone=UTC -Xmx3g

######## MPS Global Properties ########
#
# MPS Version: 11.12.1
#
# These are the properties that can be configured on the Client-side.

# Web Services
mps.web.service.url=http://localhost:9000

# Input Tray Folder for Feasibility Check (full path and located on the client side)
mps.input.tray.feasibility=/config/sfc-input

# Exchange folders for Scheduling Tool data (full path and located on the client side)
mps.input.tray.sch_tool=/tmp/mps-local-input-tray
mps.output.tray.sch_tool=/config/sfc-output

# Log location for client
mps.log.client.location=/config/logs/client.log

# Duration of the CHEOPS satellite orbit (in minutes)
mps.orbit.duration=100

# Default maximum duration of a slew maneuver (in minutes)
mps.slew.max_duration=6

# Configurable margin for the duration of the slew manoeuvre between a target and anti-sun position
mps.slew.toAntiSunDurationMargin=260

# Feasibility Check - thresholds to be used for the generation of Visits (in [0.5, 1] interval)
mps.fc.visgen.thresholds.effectiveness=0.60
mps.fc.visgen.thresholds.visit_duration=0.60

# Limit to be added to "now" time for determining the time from which manual planning shall not be allowed (in minutes)
mps.manual_planning.limit=0

# Schedule Validator - thresholds to be used for the validation of Plan (in [0.5, 1] interval)
mps.schedvalid.thresholds.effectiveness.margin=0.50

# Number of chunks for parallel processing
mps.chunks.number=22

# Maximum flexibility window duration for the visits, in minutes
mps.visgen.max_flexibility_window_duration=1440

## Parameter id for FBF_ENB which is used to enable/disable FBFs
mps.fbf_enb.parameter_id=DPT07699

## Source file context for MPS output interfaces, namely iActivityPlan and iStarMap files
mps.source_file_context=TEST

######## On-board memory profile ########

# Default contact duration with G/S per day (in minutes) 
mps.on-board-memory.default-downlink-duration=25

# Rate to simulate memory consumption for each visit(bytes per second)
mps.on-board-memory.consumption-rate=2000

# number of requests to be processed at once (in increments)
mps.requests-per-generation=10

######## Activation of workarounds ########

# Display only planned occurrences in HMI:
# One test scenario is to generate visits for a large amount of requests and long period. This 
# creates too many occurrences for HMI to handle. This workaround displays only planned visits, so
# one can generate visits, export and run the schedule solver, then import the solution back to see
# the results.
hmi.workaround.only-planned-occurrences=false

######## MPS Server Properties ########
# 
# These are the properties that can be configured on the Server-side.
#
#  IMPORTANT NOTE:
# ---------------- 
#   For Client/Server architecture, this section of the mps.global.properties 
# file must be aligned between both Components. 
#

# DB Server
mps.db.server.driver=org.postgresql.Driver
mps.db.server.url=jdbc:postgresql://localhost:5432/cheops_mps
mps.db.server.username=postgres
mps.db.server.password=postgres
mps.db.server.dialect=org.hibernate.dialect.PostgreSQLDialect
mps.db.server.schema=update

# Input and Output Trays for MPS (located on the server side!)
mps.input.tray=/opt/mps/mps-input-tray
mps.output.tray=/opt/mps/mps-output-tray

# Log location for server
mps.log.server.location=/config/logs/server.log
