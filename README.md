# CHEOPS Scheduling Feasibility Checker

The CHEOPS Scheduling Feasibility Checker (SFC) tool allows you to check whether and when a target is visible to the instrument. You can also assess the impact that interruptions due to the satellite's passage through the South Atlantic Anomaly and Earth Occultations have on the target visibility. Finally, the SFC estimates the CHEOPS Observing Efficiency for the target, i.e. the fraction of time the instrument can collect photons from the target within the visibility window.

The SFC is available as a Docker container with a web based GUI that you can run on your own computer.

# Getting started

## Installation

1. If Docker is not installed on your computer, install it following [these](https://www.docker.com/get-started) instructions. If you are using MacOS, please note that the SFC Docker container may not work with Docker version 4.27. If you have this version, please upgrade Docker to the latest version.  
2. Docker needs to be running in order to start the SFC container, so start Docker by clicking on the Docker Desktop icon.
3. Open a terminal window and navigate into a directory of your choice that will serve as the SFC's working directory.
4. In that directory, download the SFC Docker container and start it by running the command shown below. Note that if you are on Linux, you may need to preface the command with `sudo`.
   
   If you are using Linux, Mac or the Windows Powershell, run

   `docker run -d --name=cheops-sfc --platform linux/amd64 --security-opt seccomp=unconfined -p 3000:3000 -v $PWD/cheops-sfc:/config --shm-size="1gb" registry.gitlab.unige.ch/cheops/containers/cheops-sfc-webtop:2.0.0`

   If you are using the Windows terminal, run

   `docker run -d --name=cheops-sfc --platform linux/amd64 --security-opt seccomp=unconfined -p 3000:3000 -v %cd%/cheops-sfc:/config --shm-size="1gb" registry.gitlab.unige.ch/cheops/containers/cheops-sfc-webtop:2.0.0`

   The Docker container will have created, among others, the sub-directories `cheops-sfc/sfc-input` and `cheops-sfc/sfc-output`. These are the SFC's input and output directories.

## Running the SFC

1. To access the SFC, both Docker Desktop and the `cheops-sfc` Docker container needs to be running. This will be the case if you just followed the installation instructions above. Otherwise, start Docker Desktop by clicking on the Docker Desktop icon, open the Containers tab and click on the start button in the row for the `cheops-sfc` container.
2. Access the web-based desktop environment containing the SFC by opening http://localhost:3000/ in a browser. If you get an error message, please wait a few seconds and refresh the page as it may take a little while for the desktop environment to load.
3. Click on the CHEOPS SFC icon in the desktop environment to start the SFC.
4. Follow the instructions in the [CHEOPS Feasibility Checker Guidelines](https://www.cosmos.esa.int/documents/1416855/15977448/SFC_user_guide_v2.pdf/874ef60b-b8f6-8f49-881f-c2d03975f37c?t=1710174433809) for how to use the SFC.

> If you notice high CPU usage while using the SFC, you can set a limit to the number CPUs that the container can use. This is done by first deleting the `cheops-sfc` container in Docker Desktop and then rerunning the command from the [installation instructions](#installation), adding the option `--cpus X` to the command, where X is replaced with the maximum number of CPUs that the container can use. This should be a number less or equal to the actual number of CPUs on your computer.

## Stopping the SFC

To shut down the SFC desktop environment you need to stop the `cheops-sfc` Docker container. This is done by either quitting the Docker Desktop application or by specifically stopping the container by pressing the stop button in the Docker Desktop's Container tab.

# Troubleshooting

* If you are using a Mac running on Apple Silicon and you experience problems either loading the web-based desktop environment in the browser, or the SFC either does not start or it freezes during the analysis, try to uncheck the option 'Use Rosetta for x86/amd64 emulation on Apple Silicon' in the Docker Desktop settings (remember to press the "Apply & restart" button) and restart the `cheops-sfc` container in the Container tab. 

# Support

For support, please contact the <a href="mailto:cheops-pso@unige.ch">CHEOPS PSO</a>.