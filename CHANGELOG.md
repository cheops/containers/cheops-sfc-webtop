# Changelog

## cheops-sfc-webtop v2.0.0 - TBD

Upgraded to SFC 11.16.0.

## cheops-sfc-webtop v1.0.0 - 2025-02-20

First release containing SFC 11.12.1.
